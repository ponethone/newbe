const koa = require('koa');
const cors = require('@koa/cors');
const errorHandler = require('./middleware/errorHandler');
// const authenticated = require('./middleware/authenticated');
const middlewares = require('./middleware');
const router = require('./routes');
const bodyParser = require('koa-bodyparser');
const serve = require('koa-static'); // static file twy phyit tae images twy ko pr a loke loke hmr phyit loh 

const app = new koa();
app.use(serve('./images')); // serve(x). x => root directory string
app.use(middlewares.errorHandlerMiddleWare);
app.use(cors());
app.use(bodyParser());
app.use(router.routes(), router.allowedMethods());

const port = 3001;
const server = app.listen(port);
console.log(`Server is running on localhost:${port}`);

module.exports = server;