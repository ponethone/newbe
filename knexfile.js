var knex = {
    client: 'mysql',
    version: '5.7',
    connection: {
        host: '127.0.0.1', // port no default localhosts
        user: 'root', // db user name
        password: '', // db password ; default ""
        database: 'LetsVoteDB' // db name
    },
    migrations: {
        tableName: "migrations",
        directory: './db/migrations'
    },
    seeds: {
        directory: './db/seeds'
    }
};

module.exports = knex;