
exports.up = function(knex) {
    return knex.schema.createTable('state',function(t){
        t.increments('id',10).unsigned().primary();
        t.string('state_code',50).notNull();
        t.string('state_region',45).notNull();
        t.string('remark',50).nullable();
        t.string('created_user', 30).nullable().defaultTo("ADMIN"); //code requirement
        t.string('updated_user', 30).nullable(); //code requirement
        t.timestamp('created_date').notNull().defaultTo(knex.fn.now()); //code requirement
        t.timestamp('updated_date').nullable().defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP')); //code requirement
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('state');
};
