
exports.up = function(knex) {
  return knex.schema.createTable('candidates',function(t){
      t.increments('id',11).unsigned().primary();
      t.integer('party_id',11).unsigned().nullable().references('id').inTable('party').onDelete('CASCADE').index().defaultTo(1);;
      t.integer('constituency_id',11).unsigned().nullable().references('id').inTable('constituency').onDelete('CASCADE').index().defaultTo(1);
      t.string('candidates_name',45).notNull();
      // t.string('image_BLOB',255).notNull().defaultTo(knex.fn.now() + 'imageFileName');
      t.string('image_BLOB',255).notNull().defaultTo('candidate.jpg');
      t.string('election_name',100).notNull();
      t.timestamp('date_of_birth').notNull().defaultTo(knex.fn.now());;
      t.string('race',100).notNull();
      t.string('religion',100).notNull();
      t.string('NRC_NO',45).notNull();
      t.string('education',45).notNull();
      t.string('occupation',45).notNull();
      t.string('permanent_address',100).notNull();
      t.string('why_candidate',1000).notNull();
      t.string('created_user', 30).nullable().defaultTo("ADMIN"); //code requirement
      t.string('updated_user', 30).nullable(); //code requirement
      t.timestamp('created_date').notNull().defaultTo(knex.fn.now()); //code requirement
      t.timestamp('updated_date').nullable().defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP')); //code requirement
  })
};

exports.down = function(knex) {
    return knex.schema.dropTable('candidates');
};