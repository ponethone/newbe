
exports.up = function (knex) {
    return knex.schema.createTable('voterlist', function (t) {
        t.increments('id', 11).unsigned().primary();
        t.string('voter_name', 50).notNullable();
        t.string('election_name', 100).notNullable();
        t.string('NRC_NO',45).notNull();
        t.timestamp('date_of_birth').notNull().defaultTo(knex.fn.now());
        t.string('race', 100).notNullable();
        t.string('religion', 100).notNullable();
        t.string('permanent_address', 100).notNullable();
        t.string('created_user', 30).nullable().defaultTo("ADMIN"); //code requirement
        t.string('updated_user', 30).nullable(); //code requirement
        t.timestamp('created_date').notNull().defaultTo(knex.fn.now()); //code requirement
        t.timestamp('updated_date').nullable().defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'));//code requirement
    })
};

exports.down = function (knex) {
    return knex.schema.dropTable('voterlist');
};