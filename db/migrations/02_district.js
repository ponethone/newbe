
exports.up = function(knex) {
    return knex.schema.createTable('district',function(t){
        t.increments('id',10).unsigned().primary();
        t.integer('state_id',10).unsigned().nullable().references('id').inTable('state').onDelete('CASCADE').index().defaultTo(1);
        t.string('district_code',30).notNull();
        t.string('district_name',45).notNull();
        t.string('remark',50).nullable();
        t.string('created_user', 30).nullable().defaultTo("ADMIN"); 
        t.string('updated_user', 30).nullable();
        t.timestamp('created_date').notNull().defaultTo(knex.fn.now()); 
        t.timestamp('updated_date').nullable().defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'));
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('district');
};