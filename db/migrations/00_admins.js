exports.up = function(knex) {
    return knex.schema.createTable('admins',function(t){
        t.increments('id',10).unsigned().primary();
        t.string('username',50).notNull();
        t.string('fullname', 50).notNull();
        t.string('password',40).notNull();
        t.string('email',50).nullable();
        //t.string('image',255).notNull().defaultTo(Date.now() + "_" + 'admin');
        t.string('image',255).notNull().defaultTo('admin.jpg');
        t.string('login_status',50).notNull().defaultTo("INACTIVE");
        t.timestamp('created_date').notNull().defaultTo(knex.fn.now()); 
        //t.timestamp('updated_date').nullable().defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP')); 
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('admins');
};