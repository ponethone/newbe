
exports.up = function(knex) {
    return knex.schema.createTable('office',function(t){
        t.increments('id',10).unsigned().primary();
        t.integer('state_id', 11).unsigned().nullable().references('id').inTable('state').onDelete('CASCADE').index().defaultTo(1);
        t.integer('district_id', 11).unsigned().nullable().references('id').inTable('district').onDelete('CASCADE').index().defaultTo(1);
        t.integer('township_id', 11).unsigned().nullable().references('id').inTable('township').onDelete('CASCADE').index().defaultTo(1);
        t.integer('ward_id', 11).unsigned().nullable().references('id').inTable('ward').onDelete('CASCADE').index().defaultTo(1);
        t.integer('village_id', 11).unsigned().nullable().references('id').inTable('village').onDelete('CASCADE').index().defaultTo(1);
        t.integer('village_tracts_id', 11).unsigned().nullable().references('id').inTable('village_tracts').onDelete('CASCADE').index().defaultTo(1);
        t.string('office_name',50).notNull();
        t.string('address',100).notNull();
        t.integer('ph_no1',11).notNull();
        t.integer('ph_no2',11).notNull();
        t.string('email',45).notNull();
        t.string('building_type',45).notNull();
        t.integer('own_rental',10).nullable();
        t.string('created_user', 30).nullable().defaultTo("ADMIN"); //code requirement
        t.string('updated_user', 30).nullable(); //code requirement
        t.timestamp('created_date').notNull().defaultTo(knex.fn.now()); //code requirement
        t.timestamp('updated_date').nullable().defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'));//code
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('office');
};