
exports.up = function(knex) {
    return knex.schema.createTable('constituency',function(t){
       t.increments('id',11).unsigned().primary();
       t.integer('party_id',11).unsigned().nullable().references('id').inTable('party').onDelete('CASCADE').index().defaultTo(1);
       t.string('constituency_name',45).notNull();
       t.string('election_name',100).notNull();
       t.string('code1',45).notNull();
       t.string('code2',45).notNull();
       t.string('created_user', 30).nullable().defaultTo("ADMIN"); //code requirement
       t.string('updated_user', 30).nullable(); //code requirement
       t.timestamp('created_date').notNull().defaultTo(knex.fn.now()); //code requirement
       t.timestamp('updated_date').nullable().defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'));//code requirement
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('constituency');
};