
exports.up = function(knex) {
    return knex.schema.createTable('party',function(t){
        t.increments('id', 11).unsigned().primary();
        t.string('party_name', 50).notNull();
        t.integer('office_id', 11).unsigned().nullable().references('id').inTable('office').onDelete('CASCADE').index().defaultTo(1);
        t.string('constituency_name', 45).notNull();
        t.string('description', 100).notNull();
        t.string('created_user', 30).nullable().defaultTo("ADMIN"); //code requirement
        t.string('updated_user', 30).nullable(); //code requirement
        t.timestamp('created_date').notNull().defaultTo(knex.fn.now()); //code requirement
        t.timestamp('updated_date').nullable().defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'));//code requirement
        })
};

exports.down = function(knex) {
   return knex.schema.dropTable('party');
};