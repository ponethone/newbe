
exports.up = function(knex) {
    return knex.schema.createTable('village_tracts',function(t){
        t.increments('id',10).unsigned().primary();
        t.integer('township_id',10).unsigned().nullable().references('id').inTable('township').onDelete('CASCADE').index().defaultTo(1);
        t.string('village_tracts_code',30).notNull();
        t.string('village_tracts_name',45).notNull();
        t.string('remark',45).nullable();
        t.string('created_user', 30).nullable().defaultTo("ADMIN"); //code requirement
        t.string('updated_user', 30).nullable(); //code requirement
        t.timestamp('created_date').notNull().defaultTo(knex.fn.now()); //code requirement
        t.timestamp('updated_date').nullable() .defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'));//code requirement
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('village_tracts');
};