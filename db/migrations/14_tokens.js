
exports.up = function(knex) {
    return knex.schema.createTable('tokens',function(t){
        t.increments('id',10).unsigned().primary();
        t.integer('user_id',10).unsigned().nullable().references('id').inTable('users').onDelete('CASCADE').index();
        t.string('token').nullable();
        t.timestamp('created_date').notNull().defaultTo(knex.fn.now()); 
        //t.timestamp('updated_date').nullable().defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP')); 
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('tokens');
};