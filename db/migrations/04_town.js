
exports.up = function(knex) {
    return knex.schema.createTable('town',function(t){
        t.increments('id',10).unsigned().primary();
        t.integer('township_id',10).unsigned().nullable().references('id').inTable('township').onDelete('CASCADE').index().defaultTo(1);
        t.string('town_code',30).notNull();
        t.string('town_name',45).notNull();       
        t.string('remark',50).nullable();
        t.string('created_user', 30).nullable().defaultTo("ADMIN"); //code requirement
        t.string('updated_user', 30).nullable(); //code requirement
        t.timestamp('created_date').notNull().defaultTo(knex.fn.now()); //code requirement
        t.timestamp('updated_date').nullable().defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP')); //code requirement
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('town');
};