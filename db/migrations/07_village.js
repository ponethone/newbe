
exports.up = function(knex) {
  return knex.schema.createTable('village',function(t){
      t.increments('id',10).unsigned().primary();
      t.integer('village_tracts_id', 11).unsigned().nullable().references('id').inTable('village_tracts').onDelete('CASCADE').index().defaultTo(1);
      t.string('village_code',30).notNull();
      t.string('village_name',45).notNull();
      t.decimal('latitude',30).nullable();
      t.decimal('longtitude',30).nullable();
      t.string('remark',45).nullable();
      t.string('created_user', 30).nullable().defaultTo("ADMIN"); //code requirement
      t.string('updated_user', 30).nullable(); //code requirement
      t.timestamp('created_date').notNull().defaultTo(knex.fn.now()); //code requirement
      t.timestamp('updated_date').nullable().defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP')); //cod
  })
};

exports.down = function(knex) {
return knex.schema.dropTable('village');
};


// Original
// exports.up = function(knex) {
//     return knex.schema.createTable('village',function(t){
//         t.increments('id',10).unsigned().primary();
//        t.integer('village_tracts_id', 11).unsigned().notNullable().references('id').inTable('village_tracts').onDelete('CASCADE').index().defaultTo(1);
//         t.string('village_code',30).notNull();
//         t.string('village_name',45).notNull();
//         t.decimal('latitude',30).nullable();
//         t.decimal('longtitude',30).nullable();
//         t.string('remark',45).nullable();
//         t.string('created_user', 10).nullable().defaultTo("ADMIN"); //code requirement
//         t.string('updated_user', 10).nullable(); //code requirement
//         t.timestamp('created_date').notNull().defaultTo(knex.fn.now()); //code requirement
//         t.timestamp('updated_date').nullable().defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP')); //cod
//     })
//   };

// exports.down = function(knex) {
//   return knex.schema.dropTable('village');
// };