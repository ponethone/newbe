
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('township').del()
    .then(function () {
      // Inserts seed entries
      return knex('township').insert([
        {
          id: 1,
          district_id: 1,
          township_code: "025144",
          township_name: "Pakokku Township",
          remark: " "
        },
        {
          id: 2,
          district_id: 2,
          township_code: "005144",
          township_name: "Chanmyatharzi Township",
          remark: " "
        },
        {
          id: 3,
          district_id: 3,
          township_code: "095144",
          township_name: "Pyinmana Township",
          remark: " "
        },
        {
          id: 4,
          district_id: 4,
          township_code: "015144",
          township_name: "Loikaw Township",
          remark: " "
        },
        {
          id: 5,
          district_id: 5,
          township_code: "075144",
          township_name: "Pindaya Township",
          remark: " "
        },
        {
          id: 6,
          district_id: 6,
          township_code: "075144",
          township_name: "Ayadaw Township",
          remark: " "
        },
        {
          id: 7,
          district_id: 7,
          township_code: "075144",
          township_name: "Natmauk Township",
          remark: " "
        },
        {
          id: 8,
          district_id: 8,
          township_code: "075144",
          township_name: "Taunggyi Township",
          remark: " "
        },
        {
          id: 9,
          district_id: 9,
          township_code: "075144",
          township_name: "Patheingyi Township",
          remark: " "
        },
        {
          id: 10,
          district_id: 10,
          township_code: "075144",
          township_name: "Kyonpyaw Township",
          remark: " "
        },
        {
          id: 11,
          district_id: 11,
          township_code: "02341",
          township_name: "MyinmuTownship",
          remark: " "
        },
        {
          id: 12,
          district_id: 12,
          township_code: "70107",
          township_name: "Daik-U Township",
          remark: " "
        },
        {
          id: 13,
          district_id: 13,
          township_code: "14042",
          township_name: "Launglon Township",
          remark: " "
        },
        {
          id: 14,
          district_id: 14,
          township_code: "110105",
          township_name: "Mudon Township",
          remark: " "
        }
      ]);
    });
};
