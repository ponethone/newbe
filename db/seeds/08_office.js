
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('office').del()
    .then(function () {
      // Inserts seed entries
      return knex('office').insert([
        {
           id: 1,
           state_id: 1,
           district_id: 1,
           township_id: 1,
           ward_id: 1,
           village_tracts_id: 1,
           village_id: 1,
           office_name: "NLD",
           address: "Sagaing",
           ph_no1: 09804322,
           ph_no2: 09459806,
           email: "nld89@mail.com",
           building_type: "buena park",
           own_rental: 111
        },
        {  
          id: 2,
          state_id: 2,
          district_id: 2,
          township_id: 2,
          ward_id: 2,
          village_tracts_id: 2,
          village_id: 2,
          office_name: "USDP",
          address: "Sagaing",
          ph_no1: 09804322,
          ph_no2: 09459806,
          email: "usdpd10@mail.com",
          building_type: "usdp",
          own_rental: 119  
        }
      ]);
    });
};
