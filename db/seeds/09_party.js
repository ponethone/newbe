
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('party').del()
    .then(function () {
      // Inserts seed entries
      return knex('party').insert([
        {
          id: 1,
          party_name: 'National League for Democracy',
          office_id: 1,
          constituency_name: "People's Assembly(Pyithu Hlttaw)",
          description: ''
        },
        {
          id: 2,
          party_name: 'Union Solidarity and Development Party',
          office_id: 2,
          constituency_name: "People's Assembly(Pyithu Hlttaw)",
          description: ''
        }
      ]);
    });
};
