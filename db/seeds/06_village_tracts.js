

exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('village_tracts').del()
    .then(function () {
      // Inserts seed entries
      return knex('village_tracts').insert([
        {
          id: 1,
          township_id: 1,
          village_tracts_code: "025144",
          village_tracts_name: "Kan Taw",
          remark: " "
        },
        {
          id: 2,
          township_id: 2,
          village_tracts_code: "020144",
          village_tracts_name: "Kan Yat Gyi",
          remark: " "
        },
        {
          id: 3,
          township_id: 3,
          village_tracts_code: "025149",
          village_tracts_name: "Kan U",
          remark: " "
        },
        {
          id: 4,
          township_id: 4,
          village_tracts_code: "025139",
          village_tracts_name: "Htee Se Kha",
          remark: " "
        },

        {
          id: 5,
          township_id: 5,
          village_tracts_code: "225109",
          village_tracts_name: "Pan Sit",
          remark: " "
        },
        {
          id: 6,
          township_id: 6,
          village_tracts_code: "825149",
          village_tracts_name: "Htwei Ni",
          remark: " "
        },
        {
          id: 7,
          township_id: 7,
          village_tracts_code: '06014',
          village_tracts_name: 'Hpet Kun',
          remark: ''
        },
        {
          id: 8,
          township_id: 8,
          village_tracts_code: '11361',
          village_tracts_name: 'Hnaw Kone',
          remark: ''
        },
        {
          id: 9,
          township_id: 9,
          village_tracts_code: '05071',
          village_tracts_name: 'Zee Cho Kone',
          remark: ''
        },
        {
          id: 10,
          township_id: 10,
          village_tracts_code: '04121',
          village_tracts_name: 'Ywar Mon',
          remark: ''
        },
        {
          id: 11, township_id: 11,
          village_tracts_code: '02341',
          village_tracts_name: 'Nyaung Pin Kan',
          remark: ''
        },
        {
          id: 12, township_id: 12,
          village_tracts_code: '70107',
          village_tracts_name: 'Ein Chay Lay Se',
          remark: ''
        },
        {
          id: 13, township_id: 13,
          village_tracts_code: '170105',
          village_tracts_name: 'Kyaung Su',
          remark: ''
        },
        {
          id: 14, township_id: 14,
          village_tracts_code: '14042',
          village_tracts_name: 'Pandin-in',
          remark: ''
        }
      ]);
    });
};


// exports.seed = function(knex) {
//   // Deletes ALL existing entries
//   return knex('village_tracts').del()
//     .then(function () {
//       // Inserts seed entries
//       return knex('village_tracts').insert([
//         {
//           id: 1,
//           township_id: 1,
//           village_tracts_code: "025144",
//           village_tracts_name: "Kan Taw",
//           remark: " "
//         },
//         {
//           id: 2,
//           township_id: 1,
//           village_tracts_code: "020144",
//           village_tracts_name: "Kan Yat Gyi",
//           remark: " "
//         },
//         {
//           id: 3,
//           township_id: 3,
//           village_tracts_code: "025149",
//           village_tracts_name: "Kan U",
//           remark: " "
//         },
//         {
//           id: 4,
//           township_id: 4,
//           village_tracts_code: "025139",
//           village_tracts_name: "Htee Se Kha",
//           remark: " "
//         },

//         {
//           id: 5,
//           township_id: 5,
//           village_tracts_code: "225109",
//           village_tracts_name: "Pan Sit",
//           remark: " "
//         },
//         {
//           id: 6,
//           township_id: 5,
//           village_tracts_code: "825149",
//           village_tracts_name: "Htwei Ni",
//           remark: " "
//         }
//       ]);
//     });
// };
