

// const bcrypt = require('../../utilities/bcrypt')
const bcrypt = require('bcrypt');

const passwordHash1 = bcrypt.hashSync('user1pwd', 12)
const passwordHash2 = bcrypt.hashSync('user2pwd', 12)
const passwordHash3 = bcrypt.hashSync('user3pwd', 12)

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        {
          id: 1,
          username: 'user1',
          fullname: 'Alice',
          password: passwordHash1, /*passwordHash1,*/ /* 2a$10$3pwJNVCM6NKzBOD1HPZHjuFdjX2Dg771aPZNbbBw9jTUbEMS919Te*/
          email: 'user1@gmail.com',
          pic: 'user1.jpg'
        },
        {
          id: 2,
          username: 'user2',
          fullname: 'Barbie',
          password: passwordHash2, /*passwordHash2*/
          email: 'user2@gmail.com',
          pic: 'user2.jpg'
        },
        {
          id: 3,
          username: 'user3',
          fullname: 'Cathy',
          password: passwordHash3, /*passwordHash2*/
          email: 'user3@gmail.com',
          pic: 'user3.jpg'
        }
      ]);
    });
};


// Original
// const bcrypt = require('../../utilities/bcrypt')
// // const bcrypt = require('bcrypt');

// let passwordHash1 = 0, passwordHash2 = 0
// bcrypt.hash('user1').then(hash => passwordHash1 = hash);
// bcrypt.hash('user2').then(hash => passwordHash2 = hash);

// // bcrypt.hash('user1pwd', 10, function(err, hash) {
// //   // Store hash in database
// //   passwordHash1 = hash
// // });
// // bcrypt.hash('user2pwd', 10, function(err, hash) {
// //   // Store hash in database
// //   passwordHash2 = hash
// // });

// exports.seed = function(knex) {
//   // Deletes ALL existing entries
//   return knex('users').del()
//     .then(function () {
//       // Inserts seed entries
//       return knex('users').insert([
//         {
//           id: 1,
//           username: 'user1',
//           fullname: 'User 1',
//           password: 'user1pwd',/*bcrypt.hashSync("user1", 10),//*//*passwordHash1,*/ //mypassword // 2a$10$3pwJNVCM6NKzBOD1HPZHjuFdjX2Dg771aPZNbbBw9jTUbEMS919Te*/
//           email: 'user1@gmail.com',
//           pic: 'user1.jpg'
//         },
//         {
//           id: 2,
//           username: 'user2',
//           fullname: 'User 2',
//           password: 'user2pwd',/*bcrypt.hashSync("user2", 10),//*//*passwordHash2*/
//           email: 'user2@gmail.com',
//           pic: 'user2.jpg'
//         },
//         {
//           id: 3,
//           username: 'user3',
//           fullname: 'User 3',
//           password: 'user3pwd',/*bcrypt.hashSync("user2", 10),//*//*passwordHash2*/
//           email: 'user3@gmail.com',
//           pic: 'user3.jpg'
//         }
//       ]);
//     });
// };