exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('constituency').del()
    .then(function () {
      // Inserts seed entries
      return knex('constituency').insert([
        {
          id: 1,
          party_id: 1,
          constituency_name: "People's Assembly(Pyithu Hlttaw)",
          election_name: '2020 MYANMAR ELECTION', 
          code1: 'ME-2020',
          code2: 'MM-2020'
        },{
          id: 2,
          party_id: 2,
          constituency_name: "People's Assembly(Pyithu Hlttaw)",
          election_name: '2020 MYANMAR ELECTION', 
          code1: 'ME-2020',
          code2: 'MM-2020'
        }
      ]);
    });
};
