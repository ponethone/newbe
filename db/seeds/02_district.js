
exports.seed = function(knex) {
 
  return knex('district').del()
   .then(function () {
       return knex('district').insert([
         {
           id: 1,
           state_id: 1,//3,
           district_code: "02051",
           district_name: "Pakokku",
           remark: " "
         },
         {
           id: 2, 
           state_id: 2,
           district_code: '02211',
           district_name: 'Mandalay District',
           remark: ''
         },
         {
           id: 3, 
           state_id: 15,
           district_code: '02091',
           district_name: 'Dekkhina District',
           remark: ''
         },
         {
           id: 4, 
           state_id: 9,
           district_code: '02151',
           district_name: 'Loikaw District',
           remark: ''
         },
         {
           id: 5, 
           state_id: 14,
           district_code: '02081',
           district_name: 'Taunggyi District',
           remark: ''
         },
         {
           id: 6, 
           state_id: 1,
           district_code: '02301',
           district_name: 'Monywa District',
           remark: ''
         },
         {
           id: 7, 
           state_id: 1,
           district_code: '02371',
           district_name: 'Sagaing District',
           remark: 'Sagaing is the captial of the Sagaing Region'
         },
         {
           id: 8, 
           state_id: 1,
           district_code: '02261',
           district_name: 'Shwebo District',
           remark: ''
         },
         {
           id: 9, 
           state_id: 1,
           district_code: '02071',
           district_name: 'Tamu District',
           remark: ''
         },
         {
           id: 10, 
           state_id: 1,
           district_code: '02311',
           district_name: 'Yinmabin District',
           remark: ''
         },
         {
           id: 11, 
           state_id: 1,
           district_code: '02088',
           district_name: 'Naga Zone',
           remark: 'Naga is a self-administered zone in the Naga Hills'
         },
         {
           id: 12, 
           state_id: 2,
           district_code: '08011',
           district_name: 'Bago District',
           remark: ''
         },
         {
           id: 13, 
           state_id: 2,
           district_code: '08101',
           district_name: 'Taungoo District',
           remark: ''
         },
         {
           id: 14, 
           state_id: 2,
           district_code: '08151',
           district_name: 'Pyay District',
           remark: ''
         },
         {
           id: 15, 
           state_id: 2,
           district_code: '08281',
           district_name: 'Tharrawaddy District',
           remark: ''
         },
         {
           id: 16, 
           state_id: 3,
           district_code: '04211',
           district_name: 'Gangaw District',
           remark: ''
         },
         {
           id: 17, 
           state_id: 3,
           district_code: '04011',
           district_name: 'Magway District',
           remark: ''
         },
         {
           id: 18, 
           state_id: 3,
           district_code: '04021',
           district_name: 'Minbu District',
           remark: ''
         },
         {
           id: 19, 
           state_id: 3,
           district_code: '04201',
           district_name: 'Pakokku District',
           
           remark: ''
         },
         {
           id: 20, 
           state_id: 3,
           district_code: '04091',
           district_name: 'Thayet District',
           remark: ''
         }
       ]);
   });
 };
 
 
 // exports.seed = function(knex) {
 
//   return knex('district').del()
//    .then(function () {
       
//        return knex('district').insert([
//          {
//            id: 1,
//            state_id: 1,//3,
//            district_code: "02051",
//            district_name: "Pakokku",
//            remark: " "
//          },
//          {
//            id: 2, 
//            state_id: 4,
//            district_code: '02211',
//            district_name: 'Mandalay District',
//            remark: ''
//          },
//          {
//            id: 3, 
//            state_id: 15,
//            district_code: '02091',
//            district_name: 'Dekkhina District',
//            remark: ''
//          },
//          {
//            id: 4, 
//            state_id: 9,
//            district_code: '02151',
//            district_name: 'Loikaw District',
//            remark: ''
//          },
//          {
//            id: 5, 
//            state_id: 14,
//            district_code: '02081',
//            district_name: 'Taunggyi District',
//            remark: ''
//          },
//          {
//            id: 6, 
//            state_id: 1,
//            district_code: '02301',
//            district_name: 'Monywa District',
//            remark: ''
//          },
//          {
//            id: 7, 
//            state_id: 1,
//            district_code: '02371',
//            district_name: 'Sagaing District',
//            remark: 'Sagain is the captial of the Sagain Region'
//          },
//          {
//            id: 8, 
//            state_id: 1,
//            district_code: '02261',
//            district_name: 'Shwebo District',
//            remark: ''
//          },
//          {
//            id: 9, 
//            state_id: 1,
//            district_code: '02071',
//            district_name: 'Tamu District',
//            remark: ''
//          },
//          {
//            id: 10, 
//            state_id: 1,
//            district_code: '02311',
//            district_name: 'Yinmabin District',
//            remark: ''
//          },
//          {
//            id: 11, 
//            state_id: 1,
//            district_code: '02088',
//            district_name: 'Naga Zone',
//            remark: 'Naga is a self-administered zone in the Naga Hills'
//          },
//          {
//            id: 12, 
//            state_id: 2,
//            district_code: '08011',
//            district_name: 'Bago District',
//            remark: ''
//          },
//          {
//            id: 13, 
//            state_id: 2,
//            district_code: '08101',
//            district_name: 'Taungoo District',
//            remark: ''
//          },
//          {
//            id: 14, 
//            state_id: 2,
//            district_code: '08151',
//            district_name: 'Pyay District',
//            remark: ''
//          },
//          {
//            id: 15, 
//            state_id: 2,
//            district_code: '08281',
//            district_name: 'Tharrawaddy District',
//            remark: ''
//          },
//          {
//            id: 16, 
//            state_id: 3,
//            district_code: '04211',
//            district_name: 'Gangaw District',
//            remark: ''
//          },
//          {
//            id: 17, 
//            state_id: 3,
//            district_code: '04011',
//            district_name: 'Magway District',
//            remark: ''
//          },
//          {
//            id: 18, 
//            state_id: 3,
//            district_code: '04021',
//            district_name: 'Minbu District',
//            remark: ''
//          },
//          {
//            id: 19, 
//            state_id: 3,
//            district_code: '04201',
//            district_name: 'Pakokku District',
           
//            remark: ''
//          },
//          {
//            id: 20, 
//            state_id: 3,
//            district_code: '04091',
//            district_name: 'Thayet District',
//            remark: ''
//          }
//        ]);
//    });
//  };
 