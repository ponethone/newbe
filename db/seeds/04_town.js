
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('town').del()
    .then(function () {
      // Inserts seed entries
      return knex('town').insert([
        {
          id: 1,
          township_id: 1,
          town_code: "011144",
          town_name: "Pakokku",
          remark: " "
        },
        {
          id: 2,
          township_id: 2,
          town_code: "011100",
          town_name: "Chanmyatharzi",
          remark: " "
        },
        {
          id: 3,
          township_id: 3,
          town_code: "018844",
          town_name: "Pyinmana",
          remark: " "
        },
        {
          id: 4,
          township_id: 4,
          town_code: "0100144",
          town_name: "Loikaw",
          remark: " "
        },
        {
          id: 5,
          township_id: 5,
          town_code: "011166",
          town_name: "Pindaya",
          remark: " "
        },
        {
          id: 6,
          township_id: 6,
          town_code: "075144",
          town_name: "Ayadaw Township",
          remark: " "
        },
        {
          id: 7,
          township_id: 7,
          town_code: "075144",
          town_name: "Natmauk Township",
          remark: " "
        },
        {
          id: 8,
          township_id: 8,
          town_code: "075144",
          town_name: "Taunggyi Township",
          remark: " "
        },
        {
          id: 9,
          township_id: 9,
          town_code: "075144",
          town_name: "Patheingyi Township",
          remark: " "
        },
        {
          id: 10,
          township_id: 10,
          town_code: "075144",
          town_name: "Kyonpyaw Township",
          remark: " "
        },
        {
          id: 11,
          township_id: 11,
          town_code: "02341",
          town_name: "MyinmuTownship",
          remark: " "
        },
        {
          id: 12,
          township_id: 12,
          town_code: "70107",
          town_name: "Daik-U Township",
          remark: " "
        },
        {
          id: 13,
          township_id: 13,
          town_code: "14042",
          town_name: "Launglon Township",
          remark: " "
        },
        {
          id: 14,
          township_id: 14,
          town_code: "110105",
          town_name: "Mudon Township",
          remark: " "
        }
      ]);
    });
};
