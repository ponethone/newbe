
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('candidates').del()
    .then(function () {
      // Inserts seed entries
      return knex('candidates').insert([
        {
          id: 1,
          party_id: 1,
          constituency_id: 1,
          candidates_name: 'Aung San Suu Kyi',
          // image_BLOB: Date.now() + "_" + 'DawAungSanSuuKyi.png',
          image_BLOB: 'DawAungSanSuuKyi.png',
          election_name: '2020 MYANMAR ELECTION',
          // date_of_birth: "1945-06-19",
          date_of_birth: "1995-06-19", 
          race: 'Bamar',
          religion: 'Buddhism',
          NRC_NO: '12/ASS(N) 010101',
          education: 'B.A. degree in Philosophy, Politics and Economics, Oxford',
          occupation: 'State Counsellor',
          permanent_address: 'No(54) Yangon',
          // why_candidate: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin diam ante, rutrum nec elit eu, fringilla vehicula augue. In enim orci, gravida eget nisl vitae, egestas consectetur libero. Duis ex erat, rutrum nec bibendum in, maximus quis nibh. Integer semper, metus ut aliquet malesuada, libero metus molestie lorem, vitae euismod nisi neque non odio.'
          // why_candidate: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin diam ante, rutrum nec elit eu, fringilla vehicula augue. In enim orci, gravida eget nisl vitae, egestas consectetur libero. Duis ex erat, rutrum nec bibendum in, maximus quis nibh. Integer semper, metus ut aliquet malesuada, libero metus molestie lorem, vitae euismod nisi neque non odio. Vestibulum vel justo nec est pulvinar laoreet nec a erat. Vestibulum feugiat quam sed sapien ullamcorper, ut congue urna auctor. Nulla facilisi. Sed a massa sed metus imperdiet feugiat. Suspendisse a enim mauris. Aliquam vel semper mi, non aliquam purus. Nulla tincidunt at lectus sed convallis. Nulla facilisi. Ut consequat sed diam in tincidunt. Fusce quis turpis vitae ante cursus tempor non vitae erat.'
          why_candidate: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin diam ante, rutrum nec elit eu, fringilla vehicula augue. In enim orci, gravida eget nisl vitae, egestas consectetur libero. Duis ex erat, rutrum nec bibendum in, maximus quis nibh. Integer semper, metus ut aliquet malesuada, libero metus molestie lorem, vitae euismod nisi neque non odio. Vestibulum vel justo nec est pulvinar laoreet nec a erat. Vestibulum feugiat quam sed sapien ullamcorper, ut congue urna auctor. Nulla facilisi. Sed a massa sed metus imperdiet feugiat. Suspendisse a enim mauris. Aliquam vel semper mi, non aliquam purus.'
        },
        {
          id: 2,
          party_id: 2,
          constituency_id: 2,
          candidates_name: 'Candidate 2',
          // image_BLOB: Date.now() + "_" + 'DawAungSanSuuKyi.png',
          image_BLOB: 'Candidate2.png',
          election_name: '2020 MYANMAR ELECTION',
          // date_of_birth: "1945-06-19",
          date_of_birth: "1996-7-12", 
          race: 'Shan',
          religion: 'Buddhism',
          NRC_NO: '12/ASS(N) 020310',
          education: 'B.A. degree in Philosophy',
          occupation: 'Businesswoman',
          permanent_address: 'No(76) Mandalay',
          // why_candidate: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin diam ante, rutrum nec elit eu, fringilla vehicula augue. In enim orci, gravida eget nisl vitae, egestas consectetur libero. Duis ex erat, rutrum nec bibendum in, maximus quis nibh. Integer semper, metus ut aliquet malesuada, libero metus molestie lorem, vitae euismod nisi neque non odio.'
          // why_candidate: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin diam ante, rutrum nec elit eu, fringilla vehicula augue. In enim orci, gravida eget nisl vitae, egestas consectetur libero. Duis ex erat, rutrum nec bibendum in, maximus quis nibh. Integer semper, metus ut aliquet malesuada, libero metus molestie lorem, vitae euismod nisi neque non odio. Vestibulum vel justo nec est pulvinar laoreet nec a erat. Vestibulum feugiat quam sed sapien ullamcorper, ut congue urna auctor. Nulla facilisi. Sed a massa sed metus imperdiet feugiat. Suspendisse a enim mauris. Aliquam vel semper mi, non aliquam purus. Nulla tincidunt at lectus sed convallis. Nulla facilisi. Ut consequat sed diam in tincidunt. Fusce quis turpis vitae ante cursus tempor non vitae erat.'
          why_candidate: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin diam ante, rutrum nec elit eu, fringilla vehicula augue. In enim orci, gravida eget nisl vitae, egestas consectetur libero. Duis ex erat, rutrum nec bibendum in, maximus quis nibh. Integer semper, metus ut aliquet malesuada, libero metus molestie lorem, vitae euismod nisi neque non odio. Vestibulum vel justo nec est pulvinar laoreet nec a erat. Vestibulum feugiat quam sed sapien ullamcorper, ut congue urna auctor. Nulla facilisi. Sed a massa sed metus imperdiet feugiat. Suspendisse a enim mauris. Aliquam vel semper mi, non aliquam purus.'
        }
      ]);
    });
};
