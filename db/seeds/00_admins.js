
const bcrypt = require('../../utilities/bcrypt');

let passwordHash1 = 0, passwordHash2 = 0
bcrypt.hash('admin1').then(hash => passwordHash1 = hash);
bcrypt.hash('admin2').then(hash => passwordHash2 = hash);

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('admins').del()
    .then(function () {
      // Inserts seed entries
      return knex('admins').insert([
        {
          id: 1,
          username: 'admin1',
          fullname: 'Admin 1',
          password: passwordHash1, //mypassword // 2a$10$3pwJNVCM6NKzBOD1HPZHjuFdjX2Dg771aPZNbbBw9jTUbEMS919Te
          email: 'admin1@gmail.com'
        },
        {
          id: 2,
          username: 'admin2',
          fullname: 'Admin 2',
          password: passwordHash2,
          email: 'admin2@gmail.com'
        }
      ]);
    });
};