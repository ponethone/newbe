
exports.seed = function(knex) {

  return knex('state').del()
    .then(function () {      
      return knex('state').insert([
        {
          id: 1,
          state_code: 'MM-01',
          state_region: 'Sagaing',
          remark: ''
        },
        {
          id: 2,
          state_code: 'MM-02',
          state_region: 'Bago',
          remark: ''
        },
        {
          id: 3,
          state_code: 'MM-03',
          state_region: 'Magway',
          remark: ''
        },
        {
          id: 4,
          state_code: 'MM-04',
          state_region: 'Mandalay',
          remark: ''
        },
        {
          id: 5,
          state_code: 'MM-05',
          state_region: 'Tanintharyi',
          remark: ''
        },
        {
          id: 6,
          state_code: 'MM-06',
          state_region: 'Yangon',
          remark: ''
        },
        {
          id: 7,
          state_code: 'MM-07',
          state_region: 'Ayeyarwady',
          remark: ''
        },
        {
          id: 8,
          state_code: 'MM-11',
          state_region: 'Kachin',
          remark: ''
        },
        {
          id: 9,
          state_code: 'MM-12',
          state_region: 'Kayah',
          remark: ''
        },
        {
          id: 10,
          state_code: 'MM-13',
          state_region: 'Kayin',
          remark: ''
        },
        {
          id: 11,
          state_code: 'MM-14',
          state_region: 'Chin',
          remark: ''
        },
        {
          id: 12,
          state_code: 'MM-15',
          state_region: 'Mon',
          remark: ''
        },
        {
          id: 13,
          state_code: 'MM-16',
          state_region: 'Rakhine',
          remark: ''
        },
        {
          id: 14,
          state_code: 'MM-17',
          state_region: 'Shan',
          remark: ''
        },
        {
          id: 15,
          state_code: 'MM-18',
          state_region: 'Nay Pyi Taw',
          remark: ''
        }
      ]);
    });
};
