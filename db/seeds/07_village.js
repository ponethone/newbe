
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('village').del()
    .then(function () {
      // Inserts seed entries
      return knex('village').insert([
        {
          id: 1,
          village_tracts_id: 1,
          village_code: "044",
          village_name: "Kan Hla",
          remark: " "
        },
        {
          id: 2,
          village_tracts_id: 2,
          village_code: "064",
          village_name: "Kaing",
          remark: " "
        },
        {
          id: 3,
          village_tracts_id: 3,
          village_code: "045",
          village_name: "Ywar Thit",
          remark: " "
        },
        {
          id: 4,
          village_tracts_id: 4,
          village_code: "074",
          village_name: "Pan Kan",
          remark: " "
        },
        {
          id: 5,
          village_tracts_id: 5,
          village_code: "0944",
          village_name: "Tha Yet Kone",
          remark: " "
        },
        {
          id: 6,
          village_tracts_id: 6,
          village_code: "044",
          village_name: "Kyar Kone",
          remark: " "
        },
        {
          id: 7,
          village_tracts_id: 7,
          village_code: "1081",
          village_name: "Nawg Si paw",
          remark: " "
        },
        {
          id: 8,
          village_tracts_id: 8,
          village_code: "168259",
          village_name: "Mon Pyar",
          remark: " "
        },
        {
          id: 9,
          village_tracts_id: 9,
          village_code: "30104",
          village_name: "Ta Khun Taing",
          remark: " "
        },
        {
          id: 10,
          village_tracts_id: 10,
          village_code: "03021",
          village_name: "Ngaphaipi",
          remark: " "
        },
        {
          id: 11,
          village_tracts_id: 11,
          village_code: "110105",
          village_name: "Ah Khun",
          remark: " "
        },
        {
          id: 12,
          village_tracts_id: 12,
          village_code: "05071",
          village_name: "Nyaung Pin Thar",
          remark: " "
        },
        {
          id: 13,
          village_tracts_id: 13,
          village_code: "11361",
          village_name: "Chaung Wa",
          remark: " "
        },
        {
          id: 14,
          village_tracts_id: 14,
          village_code: "04121",
          village_name: "Ywar Mon Thar",
          remark: " "
        }
      ]);
    });
};
