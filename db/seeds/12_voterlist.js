
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('voterlist').del()
    .then(function () {
      // Inserts seed entries
      return knex('voterlist').insert([
        {
          id: 1, 
          voter_name: 'Yoon', 
          election_name: '2020 MYANMAR ELECTION', 
          NRC_NO: '12/PAB(N)012121',
          date_of_birth: "1999-10-15", 
          race: 'Bamar', 
          religion: 'Buddhism', 
          permanent_address: 'No(330), Yangon'
        },
        {
          id: 2, 
          voter_name: 'Kaung', 
          election_name: '2020 MYANMAR ELECTION',  
          NRC_NO: '12/TAB(N)021212',
          date_of_birth: "1999-010-17", 
          race: 'Bamar', 
          religion: 'Buddhism', 
          permanent_address: 'No(440), Yangon'
        }
      ]);
    });
};
