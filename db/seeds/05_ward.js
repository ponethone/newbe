
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('ward').del()
    .then(function () {
      // Inserts seed entries
      return knex('ward').insert([
        {
          id: 1,
          town_id: 1,
          ward_code: "0198744",
          ward_name: "Ayechantharyar No(1)",
          remark: " "
        },
        {
          id: 2,
          town_id: 2,
          ward_code: "0199944",
          ward_name: "MyoThit No(1)",
          remark: " "
        },
        {
          id: 3,
          town_id: 3,
          ward_code: "0190044",
          ward_name: "Min Ga Lar",
          remark: " "
        },
        {
          id: 4,
          town_id: 4,
          ward_code: "0190044",
          ward_name: "Shan Su",
          remark: " "
        },
        {
          id: 5,
          town_id: 5,
          ward_code: "0190044",
          ward_name: "Shan",
          remark: " "
        },
        {
          id: 6,
          town_id: 6,
          ward_code: "04121",
          ward_name: "Aung San",
          remark: " "
        },
        {
          id: 7,
          town_id: 7,
          ward_code: "10011",
          ward_name: "Pin Lon",
          remark: " "
        },
        {
          id: 8,
          town_id: 8,
          ward_code: "05011",
          ward_name: "Myo Ma",
          remark: " "
        },
        {
          id: 9,
          town_id: 9,
          ward_code: "14011",
          ward_name: "Pein Hne Taw",
          remark: " "
        },
        {
          id: 10,
          town_id: 10,
          ward_code: "06014",
          ward_name: "Pyi Taw Thar",
          remark: " "
        },
        {
          id: 11,
          town_id: 11,
          ward_code: "02371",
          ward_name: "Myauk Paing",
          remark: " "
        },
        {
          id: 12,
          town_id: 12,
          ward_code: "08011",
          ward_name: "Kyaung su",
          remark: " "
        },
        {
          id: 13,
          town_id: 13,
          ward_code: "09011",
          ward_name: "Maing Lone",
          remark: " "
        },
        {
          id: 14,
          town_id: 14,
          ward_code: "02372",
          ward_name: "Taung Paing",
          remark: " "
        }
      ]);
    });
};
