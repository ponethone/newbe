const knex = require('../knex')
const jwt = require('jsonwebtoken');
const secretKey = process.env.JWT_SECRET || 'yymk';

module.exports = async (ctx, next) => {
    if (!ctx.headers.authorization) {
        //ctx.throw(403, 'No token.');
        ctx.throw(401, "You don't have authorization! (No token) ^_^")
    }
    const f2btoken = ctx.headers.authorization.split(' ')[1];
    /* 
        For e.g.
        ctx.headers.authorization = Bearer eyJhbGcizI1NInRVCJ9.eyJz..SNIP..OjEsImzOKnGk-edQsP9QV1INJmZA
        [0] Bearer
        [1] eyJhbGcizI1NInRVCJ9.eyJz..SNIP..OjEsImzOKnGk-edQsP9QV1INJmZA
    */

   try {
        const data = jwt.verify(f2btoken, secretKey);
        //const user = await DATABASE('tokens').where({ user_id: data.id });
        const user = await knex('tokens').select('*')
            .where({ user_id: data.id })
        if (!user) {
            ctx.throw(401, "You don't have authorization!")
        }
        //ctx.request.jwtPayload = jwt.verify(f2btoken, secretKey);
        ctx.request.jwtPayload = data
   }
   catch (err) {
        ctx.throw(err.status || 403, err.text);
   }

   await next();
};