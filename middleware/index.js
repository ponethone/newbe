const authMiddleWare = require('./authenticated');
const errorHandlerMiddleWare = require('./errorHandler');

module.exports = {
    authMiddleWare,
    errorHandlerMiddleWare
}