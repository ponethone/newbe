module.exports = async (ctx, next) => {
  try {
    await next();
  } 
  catch (err) {
    ctx.type = 'json'
    ctx.status = err.status || 500;
    ctx.body = err.message;
    console.log("ctx.body in errorHandler: ", ctx.body)
    ctx.app.emit('error', err, ctx);
  }
};