const knex = require('../knex')
const jwt = require('jsonwebtoken')
const secretKey = process.env.JWT_SECRET || 'yymk';

const generateAuthToken = dbUser => {
    // Sign and return the token just like before except this time, sub is the actual database user ID.  
    const payload = { id: dbUser.id, email: dbUser.email};
    const token = jwt.sign(payload, secretKey); //AUTH_UTILS.JWT_KEY);
    return token;
}

async function processTokens (dbUser) {
    // const token =  generateAuthToken(dbUser);
    const token = generateAuthToken(dbUser)
    const checkUser = await knex('tokens').select('*')
        .where({ user_id: dbUser.id })

    if (checkUser && checkUser.length > 0) {
        console.log(`Token for user id ${dbUser.id} already exists!`)
        await knex('tokens').update(token).where({ user_id: dbUser.id })
    } 
    else {
        console.log(`Token for user id ${dbUser.id} doesn't exist!`)
        //await knex('tokens').insert({ user_id: dbUser, token });
        await knex('tokens').insert({user_id: dbUser.id, token: token});
    }
    return token
}
        
module.exports = {processTokens}

    

        