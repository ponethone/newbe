const Router = require("koa-router");
const knex = require("../../knex");
//const indexForUpload = require("../../index")
const multer = require('@koa/multer');

const router = new Router({ prefix: '/candidates' });
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `./images`)
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + "_" + file.originalname)
    }
})
const upload = multer({ storage: storage });

router.get('/', async ctx => {
    console.log("Requested url is GET method /candidates/ from Team 11");

    const data = await knex('candidates').select('*');
    ctx.body = data;
})

router.get('/:candidates_id', async ctx => {
    console.log("Requested url is GET method /users/:candidates_id");
    const paramid = parseInt(ctx.params.candidates_id);

    const data = await knex('candidates').select("*").where({ id: paramid }).first();
    ctx.body = data;
})

// router.get('/:candidates_id/', upload.single('image_BLOB'), async ctx => {
//     console.log("Requested url is GET method /candidates/:candidates_id/image_BLOB");
//     console.log('ctx.file', ctx.file); // formdata ko file pr a pr a win send lyk tr moh loh ctx.file

//     const requestBody = ctx.request.body;

//      // profile column htl ko upload loke lyk tae image file yk name htae ml
//     const data = await knex('candidates').insert({ ...requestBody, image_BLOB: ctx.file.filename })
//         .then(async res => {
//             return await knex('candidates').select("*").where({ id: res[0] }).first();
//         });

//     ctx.body = data;
// });

router.post('/', upload.single('image_BLOB'), async ctx => {
    console.log("Requested url is POST method /candidates/image_BLOB");
    console.log('ctx.file', ctx.file); // formdata ko file pr a pr a win send lyk tr moh loh ctx.file
    const requestBody = ctx.request.body;

     // profile column htl ko upload loke lyk tae image file yk name htae ml
    knex.raw('SET foreign_key_checks = 0')
    const data = await knex('candidates').insert({ ...requestBody, image_BLOB: ctx.file.filename })
        .then(async res => {
            return await knex('candidates').select("*").where({ id: res[0] }).first();
        });

    ctx.body = data;
});

router.put('/:candidates_id', async (ctx) => {
    const paramid = parseInt(ctx.params.candidates_id);
    const requestBody = ctx.request.body;
    console.log("\nYour requested route is candidates PUT");

    const data = await knex('candidates').update(requestBody).where({ id: paramid })
        .then(async () => {
            console.log("Updated candidates data:");
            return await knex('candidates').select("*").where({ id: paramid }).first();
        });
    console.log("PUT candidates:\n" + data);

    ctx.body = data;
});

router.patch('/:candidates_id/', upload.single('image_BLOB'), async ctx => {
    console.log("Requested url is PUT method /candidates/");
    console.log('ctx.file', ctx.file);
    const requestBody = ctx.request.body;
    const paramid = parseInt(ctx.params.candidates_id);

    const data = await knex('candidates').update({ ...requestBody, image_BLOB: ctx.file.filename }).where({ id: paramid })
        .then(async res => {
            return await knex('candidates').select("*").where({ id: paramid }).first();
        });

    ctx.body = data;
});

router.delete('/:candidates_id', async (ctx) => {
    const paramid = parseInt(ctx.params.candidates_id);
    console.log("\nYour requested route is candidates DELETE");

    await knex('candidates').delete().where({ id: paramid })
        .then(data => {
            console.log("Deleted candidates data:");
            console.log(data);
            ctx.body = `Deleted Id is ${paramid}`;
        });
});

module.exports = router;