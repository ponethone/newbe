const Router = require('koa-router');
const middlewares = require('../../middleware')
const Candidates = require('./Candidates');
//const Admins = require('./Admins');
const Users = require('./Users');
const authRoute = require('./AuthRoutes')
// const authRoute = require('./AuthRoutes')(app);
//const authenticated = require('../../middleware/authenticated')
const UsersByTokensRoute = require('./UsersByTokens')

const router = new Router();

// router.use(middlewares.authMiddleWare)
// router.use(Users.routes())
// router.use(authRoute.routes())
// router.use(Candidates.routes())

router.use(Users.routes()/*, middlewares.authMiddleWare*/)
router.use(authRoute.routes());
router.use(Candidates.routes()/*, middlewares.authMiddleWare*/);
//router.use(Admins.routes());
router.use(UsersByTokensRoute.routes());

module.exports = router;