const Router = require("koa-router");
const knex = require("../../knex");
//const indexForUpload = require("../../index")
const multer = require('@koa/multer');

const router = new Router({ prefix: '/users' });
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `../../images`)
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + "_" + file.originalname)
    }
})
const upload = multer({ storage: storage });

router.get('/', async ctx => {
    console.log("Requested url is GET method /users/");

    const data = await knex('users').select('*');
    ctx.body = data;
})

router.get('/:users_id', async ctx => {
    console.log("Requested url is GET method /users/:users_id");
    const paramid = parseInt(ctx.params.users_id);

    const data = await knex('users').select("*").where({ id: paramid }).first();
    ctx.body = data;
})

router.post('/', upload.single('image'), async ctx => {
    console.log("Requested url is POST method /users/");
    console.log('ctx.file', ctx.file);
    const requestBody = ctx.request.body;

    const data = await knex('users').insert({ ...requestBody, image: ctx.file.filename })
        .then(async res => {
            return await knex('users').select("*").where({ id: res[0] }).first();
        });

    ctx.body = data;
});

router.put('/:users_id', upload.single('image'), async ctx => {
    console.log("Requested url is PUT method /users/");
    console.log('ctx.file', ctx.file);
    const requestBody = ctx.request.body;
    const paramid = parseInt(ctx.params.users_id);

    const data = await knex('users').update({ ...requestBody, image: ctx.file.filename }).where({ id: paramid })
        .then(async res => {
            return await knex('users').select("*").where({ id: paramid }).first();
        });

    ctx.body = data;
});

router.delete('/:users_id', async (ctx) => {
    console.log("Your request is users DELETE");
    const paramid = parseInt(ctx.params.users_id);

    const data = await knex('users').delete().where({ id: paramid })
        .then(() => {
            return "DELETED ID IS " + paramid;
        });

    ctx.body = data;
})

module.exports = router;