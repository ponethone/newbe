// const Router = require("koa-router");
// const knex = require('../../../knex')
// // const bcrypt = require('../../../utilities/bcrypt')
// const bcrypt = require('bcrypt');
// const pt = require('../../../utilities/ProcessTokens')

// const router = new Router({ prefix: "/auth/login" });
const Router = require("koa-router");
const knex = require('../../../knex')
// const bcrypt = require('../../../utilities/bcrypt')
const bcrypt = require('bcrypt');
const pt = require('../../../utilities/ProcessTokens')

const router = new Router({ prefix: "/auth/login" });
router.post('/', async (ctx) => {
    console.log("In AuthLogin router post")
    //const requestBody = ctx.request.body;
    const { email, password } = ctx.request.body;
    console.log("your requested route is login POST");

    // if (!email) ctx.throw(422, 'email required.');
    // if (!password) ctx.throw(422, 'Password required.');

    // const dbUser = await knex('users').select('id', 'password')
    //     .whereIn('email', [email])
    //     .first()
    const dbUser = await knex('users').select('*')
        //.whereIn('email', [email])
        .where({email: email})
        .first()

    // const passwordHash = bcrypt.hashSync(password, 12)
    const wrongUserPassMsg = 'Incorrect email and/or password.';
    if (!dbUser/* || (dbUser && dbUser.length == 0*/) 
        ctx.throw(401, wrongUserPassMsg)

    if (bcrypt.compareSync(ctx.request.body.password, dbUser.password)) {
    // if (password === dbUser.password) {
        console.log("Passwords match!")

        await knex('users').update({login_status: 'ACTIVE'}).where({ email: email })
        .then(async () => {
            return await knex('users').select("*").where({ email: email }).first();
        });

        const token = pt.processTokens(dbUser)

        // const data = {
        //     id: dbUser.id,
        //     username: dbUser.username,
        //     fullname: dbUser.fullname,
        //     email: dbUser.email,
        //     //password: dbUser.password,
        //     password: dbUser.password,
        //     token
        // }
        const data = {
            user: {
                id: dbUser.id,
                username: dbUser.username,
                fullname: dbUser.fullname,
                email: dbUser.email,
                pic: dbUser.pic
                //password: dbUser.password,
            },
            authToken: token
        }

        //ctx.body = token
        ctx.body = data
    }
    else
        ctx.throw(401, wrongUserPassMsg)
});

module.exports = router;