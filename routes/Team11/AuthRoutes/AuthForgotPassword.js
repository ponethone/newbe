const Router = require("koa-router");
const knex = require('../../../knex')
// const bcrypt = require('../../../utilities/bcrypt')
const bcrypt = require('bcrypt');
const pt = require('../../../utilities/ProcessTokens')

const router = new Router({ prefix: "/auth/forgotPassword" });
router.post('/', async (ctx) => {
    console.log("In AuthForgotPassword router post")
    //const requestBody = ctx.request.body;
    const { email } = ctx.request.body;
    console.log("your requested route is forgotPassword POST");

    // if (!email) ctx.throw(422, 'email required.');
    // if (!password) ctx.throw(422, 'Password required.');

    // const dbUser = await knex('users').select('id', 'password')
    //     .whereIn('email', [email])
    //     .first()
    const dbUser = await knex('users').select('*')
        //.whereIn('email', [email])
        .where({email: email})
        .first()

    if (dbUser.length <= 0) {
        console.log("Email not found!")
        ctx.throw(404, "Email not found!")
    }

    const data = {
        id: dbUser.id,
        email: dbUser.email,
        password: dbUser.password
    }

    ctx.body = data
});

module.exports = router;