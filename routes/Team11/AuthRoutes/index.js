// const Router = require("koa-router");
// const knex = require("../../knex");
// const authLoginRoute = require('./AuthLogin');
// const authRegisterRouter = require('./AuthRegister');
// const logoutRoute = require('./AuthLogout');

// const router = new Router();

// router.post('/auth', authLoginRoute);
// //router.post('/auth', logoutRoute); // Edit this
// router.use(authLoginRoute.routes());
// router.use(authRegisterRouter.routes());
// router.use(logoutRoute.routes());

// module.exports = router;

const Router = require('koa-router')
const knex = require('../../../knex')
const authLoginRoute = require('./AuthLogin')
const authRegisterRouter = require('./AuthRegister')
const logoutRoute = require('./AuthLogout');
const forgotPasswordRoute = require('./AuthForgotPassword');
const bodyParser = require('koa-bodyparser');

const router = new Router()
router.redirect('/auth', authLoginRoute)
//router.post('/auth', authLoginRoute);
//router.post('/auth', logoutRoute); // Edit this
router.use(authLoginRoute.routes(), bodyParser());
// router.use(authRegisterRouter.routes());
router.use(authRegisterRouter.routes())//, verifySignUp.checkDuplicateUsernameOrEmail)
router.use(logoutRoute.routes());
router.use(forgotPasswordRoute.routes());

// const appFunc = function(app) {
//     // app.use(async (ctx, next) => {
//     //     ctx.header(
//     //         "Access-Control-Allow-Headers",
//     //         "x-access-token, Origin, Content-Type, Accept"
//     //     )
//     //     await next()
//     // })
//     app.use(router.routes(), router.allowedMethods());
// };

module.exports = router;
//module.exports = appFunc