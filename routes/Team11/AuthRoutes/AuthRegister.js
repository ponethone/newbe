const Router = require("koa-router");
const knex = require('../../../knex')
// const bcrypt = require('../../../utilities/bcrypt');
const bcrypt = require('bcrypt');
const pt = require('../../../utilities/ProcessTokens')

const router = new Router({ prefix: "/auth/register" });
router.post('/', async (ctx) => {
    console.log("In AuthRegister router post")
    const { username, fullname, email, password } = ctx.request.body;
        console.log("ctx.request.body", ctx.request.body)

        // await knex('users').select('*')
        //     //.whereIn('email', [email])
        //     .where({username: username})
        //     //.first()
        //     .then(user => {
        //         console.log("Check duplicate1")
        //         if (user) {
        //             ctx.throw(409, "Username is already in use!")
        //         }
        //         else
        //             console.log("Not duplicate")
        //     })
        const usernameUsers = await knex('users').select('*')
            //.whereIn('email', [email])
            .where({username: username})
            //.first()
        console.log("Duplicate user with username: ", usernameUsers)
        const emailUsers = await knex('users').select('*')
            .where({email: email})
        console.log("Duplicate user with email: ", emailUsers)
        if (usernameUsers.length > 0 & emailUsers.length > 0) {
            console.log("Both of the email and username already exist!")
            ctx.throw(409, "Both of the email and username already exist!")
        }
        else {
            if (usernameUsers.length > 0) {
                console.log("Username already exists!")
                ctx.throw(409, "Username already exists!")
            }
            else
                console.log("Not duplicate")
                
            if (emailUsers.length > 0) {
                console.log("Email already exists!")
                ctx.throw(409, "Email already exists!")
            }
            else
                console.log("Not duplicate")
        }
        
        const passwordHash = bcrypt.hashSync(password, 12)
        // Fine
        // const dbUser = await knex('users').insert({username: username, fullname: fullname, email: email, password: passwordHash});
        const userId = await knex('users').insert({username: username, fullname: fullname, email: email, password: passwordHash})
        const dbUser = await knex('users').select("*").where({ id: userId }).first();
        const token = pt.processTokens(dbUser)

        await knex('users').update({login_status: 'ACTIVE'}).where({ id: userId })
        .then(async () => {
            return await knex('users').select("*").where({ id: userId }).first();
        });

        const data = {
            user: {
                id: dbUser.id,
                username: dbUser.username, 
                fullname: dbUser.fullname,
                email: dbUser.email,
                pic: dbUser.pic
            },
            authToken: token,
        }

        console.log("dbUser Finished")
        //return res.status(200).json();
        ctx.body = data
    //     console.log("ctxBody", ctx.body)
    // try {
        
    // }
    // catch (err) {
    //     //return res.status(500).json({ message: `${JSON.stringify(error)}` });
    //     console.log("In authregister error catch")
    //     console.log(err)
    //     ctx.throw(err.status || 403, err.text);
    // }
});

module.exports = router