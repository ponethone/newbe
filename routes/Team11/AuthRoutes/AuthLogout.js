const Router = require("koa-router");
const knex = require('../../../knex')
const jwt = require('jsonwebtoken');
const bcrypt = require('../../../utilities/bcrypt');

const router = new Router({ prefix: "/auth/logout" });
// router.post('/id:', async (ctx) => {
router.post('/:id', async (ctx) => {
    console.log("In AuthLogout router post")
    const paramid = parseInt(ctx.params.id);
    // const paramid = parseInt(ctx.request.jwtPayload.id)

    await knex('users').update({login_status: 'INACTIVE'}).where({ id: paramid })
    .then(async () => {
        return await knex('users').select("*").where({ id: paramid }).first();
    });
    
    try {
        const data = await knex('tokens').delete().where({ user_id: paramid });
        ctx.body = data
        //return res.status(200).json({});
    } 
    catch (error) {
        ctx.throw(500, error)
        //return res.status(500).json({ message: `${JSON.stringify(error)}` });
    }
});

module.exports = router;