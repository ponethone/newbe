const Router = require("koa-router");
const knex = require("../../knex");

const router = new Router({ prefix: "/tokens" });
router.get('/:tokens_name', async (ctx) => {                                                                                                   
    const paramName = tokens_name
    console.log("your requested route is /tokens/:tokens_name");

    const mergeData = await knex('tokens')
        .select("tokens.token", "users.id", "users.username", "users.fullname", "users.email", "users.pic")
        //.where({id: id})
        .where({"tokens.token": paramName})
        .leftJoin('users', 'tokens.user_id', 'users.id')
        .first();

    const data = {
        user: {
            id: mergeData.id,
            username: mergeData.username,
            fullname: mergeData.fullname,
            email: mergeData.email,
            pic: mergeData.pic
        },
        authToken: mergeData.token
    }
    ctx.body = data;
});

module.exports = router;