const Router = require("koa-router");
const knex = require("../../knex");
//const indexForUpload = require("../../index")
const multer = require('@koa/multer');

const router = new Router({ prefix: '/admins' });

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `../../images`) // images so tae folder htl ko save hmr
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + "_" + file.originalname)
    }
})

const upload = multer({ storage: storage });

// http://localhost:5000/admins/244
router.get('/', async ctx => {
    console.log("Requested url is GET method /admins/");

    const data = await knex('admins').select('*');

    ctx.body = data;
})

// http://localhost:5000/admins?a=10
router.get('/:admins_id', async ctx => {
    console.log("Requested url is GET method /admins/:admins_id");

    const paramid = parseInt(ctx.params.admins_id);

    const data = await knex('admins').select("*").where({ id: paramid }).first();

    ctx.body = data;
})

router.post('/', upload.single('image'), async ctx => {
    console.log("Requested url is POST method /admins/");
    console.log('ctx.file', ctx.file); // formdata ko file pr a pr a win send lyk tr moh loh ctx.file

    const requestBody = ctx.request.body;

     // profile column htl ko upload loke lyk tae image file yk name htae ml
    const data = await knex('admins').insert({ ...requestBody, image: ctx.file.filename })
        .then(async res => {
            return await knex('admins').select("*").where({ id: res[0] }).first();
        });

    ctx.body = data;
});

// admins_id
router.put('/:admins_id', upload.single('image'), async ctx => {
    console.log("Requested url is PUT method /admins/");
    console.log('ctx.file', ctx.file);

    const requestBody = ctx.request.body;
    const paramid = parseInt(ctx.params.admins_id);

    const data = await knex('admins').update({ ...requestBody, image: ctx.file.filename }).where({ id: paramid })
        .then(async res => {
            return await knex('admins').select("*").where({ id: paramid }).first();
        });

    ctx.body = data;
});

router.delete('/:admins_id', async (ctx) => {
    console.log("Your request is admins DELETE");

    const paramid = parseInt(ctx.params.admins_id);

    const data = await knex('admins').delete().where({ id: paramid })
        .then(() => {
            return "DELETED ID IS " + paramid;
        });

    ctx.body = data;
})

module.exports = router;