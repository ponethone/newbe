const Router = require('koa-router');
// const Team1Route = require('./Team1');
// const Team3Route = require('./Team3');
// const Team4Route = require('./Team4');
const Team6Route = require('./Team6');
const Team11Route = require('./Team11');
const middlewares = require('../middleware');
//const authenticated = require('../middleware/authenticated');

const router = new Router();

// router.use(Team1Route.routes());
// router.use(Team3Route.routes());
// router.use(Team4Route.routes());
router.use(Team6Route.routes()/*, middlewares.authMiddleWare*/);
router.use(Team11Route.routes());

module.exports = router;