const Router = require("koa-router");
const knex = require("../../knex");

const router = new Router({ prefix: "/ward" });
router.get('/', async (ctx) => {
    console.log("your requested route is /ward");

    const data = await knex('ward')
        .select('ward.*', 'state.state_region', 'district.district_name', 'township.township_name', 'town.town_name')
        //.from('township', 'district', 'state')
        .leftJoin('town', 'ward.town_id', 'town.id')
        .join('township', 'town.township_id', 'township.id')
        .join('district', 'township.district_id', 'district.id')
        .join('state', 'district.state_id', 'state.id')

    ctx.body = data;
});

router.get('/:ward_id', async (ctx) => {                                                                                                   
    const paramid = parseInt(ctx.params.ward_id);
    console.log("your requested route is /ward/:ward_id");

    const data = await knex('ward')
        .select('ward.*', 'state.state_region', 'district.district_name', 'township.township_name', 'town.town_name')
        //.from('township', 'district', 'state')
        .where({"ward.id": paramid })
        .leftJoin('town', 'ward.town_id', 'town.id')
        .join('township', 'town.township_id', 'township.id')
        .join('district', 'township.district_id', 'district.id')
        .join('state', 'district.state_id', 'state.id')
        .first()

    ctx.body = data;
});

router.delete('/:ward_id', async (ctx) => {
    const paramid = parseInt(ctx.params.ward_id);
    console.log("your requested route is ward DELETE");

    const data = await knex('ward').delete().where({ id: paramid });
    ctx.body = "Deleted Id is ${paramid}";
});

router.put('/:ward_id', async (ctx) => {
    const paramid = parseInt(ctx.params.ward_id);
    const requestBody = ctx.request.body;
    console.log("your requested route is ward PUT");

    console.log("Before wardColumns")
    const wardColumns = ['ward_code', 'ward_name', 'remark', 'updated_user'];
    console.log("Before ColumnsfromOtherTables")

    let wardData = Object.keys(requestBody) 
    .filter(key => wardColumns.includes(key))
    .reduce((obj, key) => {
        return {
        ...obj,
        [key]: requestBody[key]
        };
    }, {});

    console.log("Filtered inserted town excluding state_region, district_name, and town_name: ", wardData)

    const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    const didFromDistrict = (await knex('district').select("id").where({ state_id: sidFromState, district_name: requestBody.district_name }).first()).id; // Obj.id
    const tsidFromTownship = (await knex('township').select("id").where({ district_id: didFromDistrict, township_name: requestBody.township_name }).first()).id;
    const tidFromTown = (await knex('town').select("id").where({ township_id: tsidFromTownship, town_name: requestBody.town_name }).first()).id;

    wardData = {...wardData, town_id: tidFromTown}
    console.log("inserted township including state_id, district_id foreign key: ", wardData)

    const data = await knex('ward').update(wardData).where({ id: paramid })
        .then(async () => {
            return await knex('ward').select("*").where({ id: paramid }).first();
        });

    ctx.body = data;
});

router.post('/', async (ctx) => {
    const requestBody = ctx.request.body;
    console.log("your requested route is ward POST");

    console.log("Before wardColumns")
    const wardColumns = ['ward_code', 'ward_name', 'remark'];
    console.log("Before ColumnsfromOtherTables")

    let wardData = Object.keys(requestBody) 
    .filter(key => wardColumns.includes(key))
    .reduce((obj, key) => {
        return {
        ...obj,
        [key]: requestBody[key]
        };
    }, {});

    console.log("Filtered inserted town excluding state_region, district_name, and town_name: ", wardData)

    const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    const didFromDistrict = (await knex('district').select("id").where({ state_id: sidFromState, district_name: requestBody.district_name }).first()).id; // Obj.id
    const tsidFromTownship = (await knex('township').select("id").where({ district_id: didFromDistrict, township_name: requestBody.township_name }).first()).id;
    const tidFromTown = (await knex('town').select("id").where({ township_id: tsidFromTownship, town_name: requestBody.town_name }).first()).id;

    wardData = {...wardData, town_id: tidFromTown}
    console.log("inserted township including state_id, district_id foreign key: ", wardData)

    const data = await knex('ward').insert(wardData);
    ctx.body = data;
});

module.exports = router;