const Router = require("koa-router");
const knex = require("../../knex");

const router = new Router({ prefix: "/district" });
router.get('/', async (ctx) => {
    console.log("your requested route is /district");

    const data = await knex('district')
        .select('district.*', 'state.state_region as state_region')
        .leftJoin('state', 'district.state_id', 'state.id')
    
    console.log("Distirct data: ", data)
    ctx.body = data;
});

router.get('/:id', async (ctx) => {                                                                                                   
    const paramid = parseInt(ctx.params.id);
    console.log("your requested route is /district/:district_id");

    const data = await knex('district')
        .select("district.*", "state.state_region as state_region")
        //.where({id: id})
        .where({"district.id": paramid})
        .leftJoin('state', 'district.state_id', 'state.id')
        .first();

    ctx.body = data;
});

router.delete('/:district_id', async (ctx) => {
    const paramid = parseInt(ctx.params.district_id);
    console.log("your requested route is district DELETE");

    const data = await knex('district').delete().where({ id: paramid });
    // .then(() => {
    //     return "Deleted Id is " + paramid;
    // })

    ctx.body = "Deleted Id is ${paramid}";
});

router.put('/:district_id', async (ctx) => {
    const paramid = parseInt(ctx.params.district_id);
    const requestBody = ctx.request.body;
    console.log("your requested route is district PUT");

    // Removing state_region column/field
    const districtColumns = ['district_code', 'district_name', 'remark', 'updated_user'];
    const stateColumns = ['state_region'];

    let districtData = Object.keys(requestBody) 
    .filter(key => districtColumns.includes(key))
    .reduce((obj, key) => {
        return {
        ...obj,
        [key]: requestBody[key]
        };
    }, {});

    insertedStateRegion = Object.keys(requestBody)
    .filter(key => stateColumns.includes(key))
    .reduce((obj, key) => {
        return {
        ...obj,
        [key]: requestBody[key]
        };
    }, {});
    console.log("Filtered inserted district excluding state_region: ", districtData)
    console.log("insertedStateRegion: ", insertedStateRegion)

    const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    districtData = {...districtData, state_id: sidFromState}
    console.log("inserted district including state_id foreign key: ", districtData)
    console.log("sidFromState: ", sidFromState)

    const data = await knex('district').update(districtData).where({ id: paramid })
        .then(async () => {
            return await knex('district').select("*").where({ id: paramid }).first();
        });

    ctx.body = data;
});

router.post('/', async (ctx) => {
    const requestBody = ctx.request.body; // including state_region
    console.log("your requested route is district POST");
    console.log("requestBody is: ", requestBody)

    const districtColumns = ['district_code', 'district_name', 'remark'];
    const stateColumns = ['state_region'];

    let districtData = Object.keys(requestBody) 
    .filter(key => districtColumns.includes(key))
    .reduce((obj, key) => {
        return {
        ...obj,
        [key]: requestBody[key]
        };
    }, {});

    insertedStateRegion = Object.keys(requestBody)
    .filter(key => stateColumns.includes(key))
    .reduce((obj, key) => {
        return {
        ...obj,
        [key]: requestBody[key]
        };
    }, {});
    console.log("Filtered inserted district excluding state_region: ", districtData)
    console.log("insertedStateRegion: ", insertedStateRegion)

    const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    districtData = {...districtData, state_id: sidFromState}
    console.log("inserted district including state_id foreign key: ", districtData)
    console.log("sidFromState: ", sidFromState)

    const data = await knex('district').insert(districtData);
    ctx.body = data;
});

module.exports = router;