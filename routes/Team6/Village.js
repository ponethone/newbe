const Router = require("koa-router");
const knex = require("../../knex");

const router = new Router({ prefix: "/village" });
router.get('/', async (ctx) => {
    console.log("your requested route is /village");

    const data = await knex('village')
        .select('village.*', 'village_tracts.village_tracts_name', 'township.township_name', 'district.district_name', 'state.state_region')
        .leftJoin('village_tracts', 'village.village_tracts_id', 'village_tracts.id')
        .join('township', 'village_tracts.township_id', 'township.id')
        .join('district', 'township.district_id', 'district.id')
        .join('state', 'district.state_id', 'state.id');

    ctx.body = data;
});

router.get('/:village_id', async (ctx) => {
    const paramid = parseInt(ctx.params.village_id);
    console.log("your requested route is /village/:village_id");

    const data = await knex('village').select("*").where({ id: paramid }).first();
    ctx.body = data;
});

router.delete('/:village_id', async (ctx) => {
    const paramid = parseInt(ctx.params.village_id);
    console.log("your requested route is village DELETE");

    const data = await knex('village').delete().where({ id: paramid });
    ctx.body = "Deleted Id is ${paramid}";
});

router.put('/:village_id', async (ctx) => {
    const paramid = parseInt(ctx.params.village_id);
    const requestBody = ctx.request.body;
    console.log("your requested route is village PUT");

    console.log("Before villageColumns")
    const villageColumns = ['village_code', 'village_name', 'remark', 'updated_user'];
    console.log("Before ColumnsfromOtherTables")

    let villageData = Object.keys(requestBody) 
    .filter(key => villageColumns.includes(key))
    .reduce((obj, key) => {
        return {
        ...obj,
        [key]: requestBody[key]
        };
    }, {});

    console.log("Filtered inserted town excluding state_region, district_name, and town_name: ", villageData)

    const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    const didFromDistrict = (await knex('district').select("id").where({ state_id: sidFromState, district_name: requestBody.district_name }).first()).id; // Obj.id
    const tsidFromTownship = (await knex('township').select("id").where({ district_id: didFromDistrict, township_name: requestBody.township_name }).first()).id;
    const vtidFromVillage_Tracts = (await knex('village_tracts').select("id").where({ township_id: tsidFromTownship, village_tracts_name: requestBody.village_tracts_name }).first()).id;

    villageData = {...villageData, village_tracts_id: vtidFromVillage_Tracts}
    console.log("inserted township including state_id, district_id foreign key: ", villageData)

    const data = await knex('village').update(villageData).where({ id: paramid })
        .then(async () => {
            return await knex('village').select("*").where({ id: paramid }).first();
        });

    ctx.body = data;
});

router.post('/', async (ctx) => {
    const requestBody = ctx.request.body;
    console.log("your requested route is village POST");

    console.log("Before villageColumns")
    const villageColumns = ['village_code', 'village_name', 'remark'];
    console.log("Before ColumnsfromOtherTables")

    let villageData = Object.keys(requestBody) 
    .filter(key => villageColumns.includes(key))
    .reduce((obj, key) => {
        return {
        ...obj,
        [key]: requestBody[key]
        };
    }, {});

    console.log("Filtered inserted town excluding state_region, district_name, and town_name: ", villageData)

    const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    const didFromDistrict = (await knex('district').select("id").where({ state_id: sidFromState, district_name: requestBody.district_name }).first()).id; // Obj.id
    const tsidFromTownship = (await knex('township').select("id").where({ district_id: didFromDistrict, township_name: requestBody.township_name }).first()).id;
    const vtidFromVillage_Tracts = (await knex('village_tracts').select("id").where({ township_id: tsidFromTownship, village_tracts_name: requestBody.village_tracts_name }).first()).id;

    villageData = {...villageData, village_tracts_id: vtidFromVillage_Tracts}
    console.log("inserted township including state_id, district_id foreign key: ", villageData)

    const data = await knex('village').insert(villageData);
    ctx.body = data;
});

module.exports = router;