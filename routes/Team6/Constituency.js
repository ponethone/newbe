const Router = require("koa-router");
const knex = require("../../knex");

const router = new Router({ prefix: "/constituency" });
router.get('/', async (ctx) => {
    console.log("your requested route is /constituency");

    const data = await knex('constituency')
        .select('constituency.*', 'party.party_name', 'office.office_name', 'village.village_name', 'village_tracts.village_tracts_name', 'ward.ward_name', 'town.town_name', 'township.township_name', 'district.district_name', 'state.state_region')
        //.from('township', 'district', 'state')
        .leftJoin('party', 'constituency.party_id', 'party.id')
        .join('office', 'party.office_id', 'office.id')
        .join('village', 'office.village_id', 'village.id')
        .join('village_tracts', 'village.village_tracts_id', 'village_tracts.id')
        .join('ward', 'office.ward_id', 'ward.id')
        .join('town', 'ward.town_id', 'town.id')
        .join('township', 'town.township_id', 'township.id')
        .join('district', 'township.district_id', 'district.id')
        .join('state', 'state.id', 'district.state_id')

    ctx.body = data;
});

router.get('/:constituency_id', async (ctx) => {                                                                                                   
    const paramid = parseInt(ctx.params.constituency_id);
    console.log("your requested route is /constituency/:constituency_id");

    const data = await knex('constituency').select("*").where({ id: paramid }).first();
    ctx.body = data;

});

router.delete('/:constituency_id', async (ctx) => {
    const paramid = parseInt(ctx.params.constituency_id);
    console.log("your requested route is constituency DELETE");

    const data = await knex('constituency').delete().where({ id: paramid });
    ctx.body = "Deleted Id is ${paramid}";
});

router.put('/:constituency_id', async (ctx) => {
    const paramid = parseInt(ctx.params.constituency_id);
    const requestBody = ctx.request.body;
    console.log("your requested route is constituency PUT");

    console.log("Before wardColumns")
    const constituencyColumns = ['constituency_name', 'election_name', 'code1', 'code2', 'updated_user'];
    console.log("Before ColumnsfromOtherTables")

    let constituencyData = Object.keys(requestBody) 
    .filter(key => constituencyColumns.includes(key))
    .reduce((obj, key) => {
        return {
        ...obj,
        [key]: requestBody[key]
        };
    }, {});

    console.log("Filtered inserted town excluding state_region, district_name, and town_name: ", constituencyData)
    console.log("Request data: ", requestBody)

    // const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    // const didFromDistrict = (await knex('district').select("id").where({ district_name: requestBody.district_name }).first()).id; // Obj.id
    // const tsidFromTownship = (await knex('township').select("id").where({ township_name: requestBody.township_name }).first()).id;
    // const tidFromTown = (await knex('town').select("id").where({ township_id: tsidFromTownship, town_name: requestBody.town_name }).first()).id;
    // const widFromWard = (await knex('ward').select("id").where({ town_id: tidFromTown, ward_name: requestBody.ward_name }).first()).id;
    // const vtidFromVillage_tracts = (await knex('village_tracts').select("id").where({ village_tracts_name: requestBody.village_tracts_name }).first()).id;
    // const vidFromVillage = (await knex('village').select("id").where({ village_name: requestBody.village_name }).first()).id;
    // //const pidFromParty = (await knex('party').select("id").where({ party_name: requestBody.party_name }).first()).id;
    // const oidFromOffice = (await knex('office').select("id").where({ state_id: sidFromState, district_id: didFromDistrict, township_id: tsidFromTownship, ward_id: widFromWard, village_tracts_id: vtidFromVillage_tracts, village_id: vidFromVillage }).first()).id
    // const pidFromParty = (await knex('party').select("id").where({ office_id: oidFromOffice, party_name: requestBody.party_name }).first()).id

    const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    const didFromDistrict = (await knex('district').select("id").where({ district_name: requestBody.district_name }).first()).id; // Obj.id
    const tsidFromTownship = (await knex('township').select("id").where({ township_name: requestBody.township_name }).first()).id;
    const tidFromTown = (await knex('town').select("id").where({ township_id: tsidFromTownship, town_name: requestBody.town_name }).first()).id;
    const widFromWard = (await knex('ward').select("id").where({ town_id: tidFromTown, ward_name: requestBody.ward_name }).first()).id;
    const vtidFromVillage_tracts = (await knex('village_tracts').select("id").where({ township_id: tsidFromTownship, village_tracts_name: requestBody.village_tracts_name }).first()).id;
    const vidFromVillage = (await knex('village').select("id").where({ village_tracts_id:vtidFromVillage_tracts, village_name: requestBody.village_name }).first()).id;
    //const pidFromParty = (await knex('party').select("id").where({ party_name: requestBody.party_name }).first()).id;
    const oidFromOffice = (await knex('office').select("id").where({ state_id: sidFromState, district_id: didFromDistrict, township_id: tsidFromTownship, ward_id: widFromWard, village_tracts_id: vtidFromVillage_tracts, village_id: vidFromVillage }).first()).id
    const pidFromParty = (await knex('party').select("id").where({ office_id: oidFromOffice, party_name: requestBody.party_name }).first()).id

    constituencyData = {
        ...constituencyData, 
        party_id: pidFromParty
    }
    console.log("inserted township including state_id, district_id foreign key: ", constituencyData)

    const data = await knex('constituency').update(constituencyData).where({ id: paramid })
        .then(async () => {
            return await knex('constituency').select("*").where({ id: paramid }).first();
        });

    ctx.body = data;
});

router.post('/', async (ctx) => {
    const requestBody = ctx.request.body;
    console.log("your requested route is constituency POST");

    console.log("Before wardColumns")
    const constituencyColumns = ['constituency_name', 'election_name', 'code1', 'code2'];
    console.log("Before ColumnsfromOtherTables")

    let constituencyData = Object.keys(requestBody) 
    .filter(key => constituencyColumns.includes(key))
    .reduce((obj, key) => {
        return {
        ...obj,
        [key]: requestBody[key]
        };
    }, {});

    console.log("Filtered inserted town excluding state_region, district_name, and town_name: ", constituencyData)
    console.log("Request data: ", requestBody)

    // const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    // console.log('sidFromState', sidFromState)
    // const didFromDistrict = (await knex('district').select("id").where({ district_name: requestBody.district_name }).first()).id; // Obj.id
    // console.log('didFromDistrict', didFromDistrict)
    // const tsidFromTownship = (await knex('township').select("id").where({ township_name: requestBody.township_name }).first()).id;
    // console.log('tsidFromTownship', tsidFromTownship)
    // const tidFromTown = (await knex('town').select("id").where({ township_id: tsidFromTownship, town_name: requestBody.town_name }).first()).id;
    // console.log('tidFromTown', tidFromTown)
    // const widFromWard = (await knex('ward').select("id").where({ town_id: tidFromTown, ward_name: requestBody.ward_name }).first()).id;
    // console.log('widFromWard', widFromWard)
    // const vtidFromVillage_tracts = (await knex('village_tracts').select("id").where({ village_tracts_name: requestBody.village_tracts_name }).first()).id;
    // console.log('vtidFromVillage_tracts', vtidFromVillage_tracts)
    // const vidFromVillage = (await knex('village').select("id").where({ village_name: requestBody.village_name }).first()).id;
    // console.log('vidFromVillage', vidFromVillage)
    // //const pidFromParty = (await knex('party').select("id").where({ party_name: requestBody.party_name }).first()).id;
    // const oidFromOffice = (await knex('office').select("id").where({ state_id: sidFromState, district_id: didFromDistrict, township_id: tsidFromTownship, ward_id: widFromWard, village_tracts_id: vtidFromVillage_tracts, village_id: vidFromVillage }).first()).id;
    // console.log('oidFromOffice', oidFromOffice)
    // const pidFromParty = (await knex('party').select("id").where({ office_id: oidFromOffice, party_name: requestBody.party_name }).first()).id
    // console.log('pidFromParty', pidFromParty)

    const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    console.log('sidFromState', sidFromState)
    const didFromDistrict = (await knex('district').select("id").where({ district_name: requestBody.district_name }).first()).id; // Obj.id
    console.log('didFromDistrict', didFromDistrict)
    const tsidFromTownship = (await knex('township').select("id").where({ township_name: requestBody.township_name }).first()).id;
    console.log('tsidFromTownship', tsidFromTownship)
    const tidFromTown = (await knex('town').select("id").where({ township_id: tsidFromTownship, town_name: requestBody.town_name }).first()).id;
    console.log('tidFromTown', tidFromTown)
    const widFromWard = (await knex('ward').select("id").where({ town_id: tidFromTown, ward_name: requestBody.ward_name }).first()).id;
    console.log('widFromWard', widFromWard)
    const vtidFromVillage_tracts = (await knex('village_tracts').select("id").where({ township_id: tsidFromTownship, village_tracts_name: requestBody.village_tracts_name }).first()).id;
    console.log('vtidFromVillage_tracts', vtidFromVillage_tracts)
    const vidFromVillage = (await knex('village').select("id").where({ village_tracts_id: vtidFromVillage_tracts, village_name: requestBody.village_name }).first()).id;
    console.log('vidFromVillage', vidFromVillage)
    //const pidFromParty = (await knex('party').select("id").where({ party_name: requestBody.party_name }).first()).id;
    const oidFromOffice = (await knex('office').select("id").where({ state_id: sidFromState, district_id: didFromDistrict, township_id: tsidFromTownship, ward_id: widFromWard, village_tracts_id: vtidFromVillage_tracts, village_id: vidFromVillage }).first()).id;
    console.log('oidFromOffice', oidFromOffice)
    const pidFromParty = (await knex('party').select("id").where({ office_id: oidFromOffice, party_name: requestBody.party_name }).first()).id
    console.log('pidFromParty', pidFromParty)

    constituencyData = {
        ...constituencyData, 
        party_id: pidFromParty
    }
    console.log("inserted township including state_id, district_id foreign key: ", constituencyData)

    const data = await knex('constituency').insert(constituencyData);
    ctx.body = data;
});

module.exports = router;