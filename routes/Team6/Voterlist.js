const Router = require("koa-router");
const knex = require("../../knex");

const router = new Router({ prefix: "/voterlist" });
router.get('/', async (ctx) => {
    console.log("your requested route is /voterlist");

    const data = await knex('voterlist').select("*");
    ctx.body = data;
});

router.get('/:voterlist_id', async (ctx) => {                                                                                                   
    const paramid = parseInt(ctx.params.voterlist_id);
    console.log("your requested route is /voterlist/:voterlist_id");

    const data = await knex('voterlist').select("*").where({ id: paramid }).first();
    ctx.body = data;
});

router.delete('/:voterlist_id', async (ctx) => {
    const paramid = parseInt(ctx.params.voterlist_id);
    console.log("your requested route is voterlist DELETE");

    const data = await knex('voterlist').delete().where({ id: paramid });
    ctx.body = "Deleted Id is ${paramid}";
});

router.put('/:voterlist_id', async (ctx) => {
    const paramid = parseInt(ctx.params.voterlist_id);
    const requestBody = ctx.request.body;
    console.log("your requested route is voterlist PUT");

    const data = await knex('voterlist').update(requestBody).where({ id: paramid })
        .then(async () => {
            return await knex('voterlist').select("*").where({ id: paramid }).first();
        });

    ctx.body = data;
});

router.post('/', async (ctx) => {
    const requestBody = ctx.request.body;
    console.log("your requested route is voterlist POST");

    const data = await knex('voterlist').insert(requestBody);
    ctx.body = data;
});

module.exports = router;