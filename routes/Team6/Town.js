const Router = require("koa-router");
const knex = require("../../knex");

const router = new Router({ prefix: "/town" });
router.get('/', async (ctx) => {
    console.log("your requested route is /town");

    const data = await knex('town')
        .select('town.*', 'township.township_name', 'district.district_name', 'state.state_region')
        //.from('township', 'district', 'state')
        .leftJoin('township', 'town.township_id', 'township.id')
        .join('district', 'district.id', 'township.district_id')
        .join('state', 'state.id', 'district.state_id')

    ctx.body = data;
});

router.get('/:town_id', async (ctx) => {                                                                                                   
    const paramid = parseInt(ctx.params.town_id);
    console.log("your requested route is /town/:town_id");

    const data = await knex('town')
        .select('town.*', 'township.township_name', 'district.district_name', 'state.state_region')
        //.from('township', 'district', 'state')
        .where({id: paramid})
        .leftJoin('township', 'town.township_id', 'township.id')
        .join('district', 'district.id', 'township.district_id')
        .join('state', 'state.id', 'district.state_id')

    ctx.body = data;
});

router.delete('/:town_id', async (ctx) => {
    const paramid = parseInt(ctx.params.town_id);
    console.log("your requested route is town DELETE");

    const data = await knex('town').delete().where({ id: paramid });
    ctx.body = "Deleted Id is ${paramid}";

});

router.put('/:town_id', async (ctx) => {
    const paramid = parseInt(ctx.params.town_id);
    const requestBody = ctx.request.body;
    console.log("your requested route is town PUT");

    console.log("Before townshipColumns")
    const townColumns = ['town_code', 'town_name', 'remark', 'updated_user'];
    console.log("Before ColumnsfromOtherTables")

    let townData = Object.keys(requestBody) 
    .filter(key => townColumns.includes(key))
    .reduce((obj, key) => {
        return {
        ...obj,
        [key]: requestBody[key]
        };
    }, {});

    console.log("Filtered inserted town excluding state_region, district_name, and township_name: ", townData)

    const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    const didFromDistrict = (await knex('district').select("id").where({ state_id: sidFromState, district_name: requestBody.district_name }).first()).id; // Obj.id
    const tsidFromTownship = (await knex('township').select("id").where({ district_id: didFromDistrict, township_name: requestBody.township_name }).first()).id;

    townData = {...townData, township_id: tsidFromTownship}
    console.log("inserted township including state_id, district_id foreign key: ", townData)

    const data = await knex('town').update(townData).where({ id: paramid })
        .then(async () => {
            return await knex('town').select("*").where({ id: paramid }).first();
        });

    ctx.body = data;
});

router.post('/', async (ctx) => {
    const requestBody = ctx.request.body;
    console.log("your requested route is town POST");

    console.log("Before townshipColumns")
    const townColumns = ['town_code', 'town_name', 'remark'];
    console.log("Before ColumnsfromOtherTables")
    const columnsfromOtherTables = ['state_region', 'township_name', 'district_name'];

    let townData = Object.keys(requestBody) 
    .filter(key => townColumns.includes(key))
    .reduce((obj, key) => {
        return {
        ...obj,
        [key]: requestBody[key]
        };
    }, {});

    console.log("Filtered inserted town excluding state_region, district_name, and town_name: ", townData)

    const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    const didFromDistrict = (await knex('district').select("id").where({ state_id: sidFromState, district_name: requestBody.district_name }).first()).id; // Obj.id
    const tsidFromTownship = (await knex('township').select("id").where({ district_id: didFromDistrict, township_name: requestBody.township_name }).first()).id;

    townData = {...townData, township_id: tsidFromTownship}
    console.log("inserted township including state_id, district_id foreign key: ", townData)

    const data = await knex('town').insert(townData);
    ctx.body = data;
});

module.exports = router;