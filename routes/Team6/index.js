const Router = require('koa-router');
const State = require('./State');
const District = require('./District');
const TownShip = require('./Township');
const Town = require('./Town');
const Ward = require('./Ward');
const Village_Tracts = require('./Village_Tracts');
const Village = require('./Village');
const Office = require('./Office');
const Party = require("./Party")
const Constituency = require('./Constituency');
const VoterList = require('./Voterlist');

const router = new Router();
router.use(State.routes())
router.use(District.routes())
router.use(TownShip.routes())
router.use(Town.routes())
router.use(Ward.routes())
router.use(Village_Tracts.routes())
router.use(Village.routes());
router.use(Office.routes())
router.use(Party.routes())
router.use(Constituency.routes())
router.use(VoterList.routes())

module.exports = router;