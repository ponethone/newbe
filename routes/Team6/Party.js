const Router = require("koa-router");
const knex = require("../../knex");
const middleware = require('../../middleware')

const router = new Router({ prefix: "/party" });
router.get('/', async (ctx) => {
    console.log("\nYour requested route is /party");

    // const data = await knex('party').select("*");

    await knex('party').select("*")
        .then(data => {
            console.log("GET party:");
            console.log(data);
            ctx.body = data;
        });


    // ctx.body = data;
});

router.get('/:party_id', async (ctx) => {                                                                                                   
    const paramid = parseInt(ctx.params.party_id);
    console.log("\nYour requested route is /party/:party_id");

    await knex('party').select("*").where({ id: paramid }).first()
        .then(data => {
            console.log("GET party:");
            console.log(data);
            ctx.body = data;
        });
});

router.delete('/:party_id', async (ctx) => {
    const paramid = parseInt(ctx.params.party_id);
    console.log("\nYour requested route is party DELETE");

    await knex('party').delete().where({ id: paramid })
        .then(data => {
            console.log("Deleted party data:");
            console.log(data);
            ctx.body = `Deleted Id is ${paramid}`;
        });
});

router.put('/:party_id', async (ctx) => {
    const paramid = parseInt(ctx.params.party_id);
    const requestBody = ctx.request.body;
    console.log("\nYour requested route is party PUT");

    const data = await knex('party').update(requestBody).where({ id: paramid })
        .then(async () => {
            console.log("Updated party data:");
            return await knex('party').select("*").where({ id: paramid }).first();
        });
    console.log("PUT party:\n" + data);

    ctx.body = data;
});

router.post('/', async (ctx) => {
    const requestBody = ctx.request.body;
    console.log("\nYour requested route is party POST");

    const data = await knex('party').insert(requestBody)
        .then(async () => {
            console.log("Added/Inserted party data:");
            return await knex('party').select("*").where({ id: paramid }).first();
        });

    ctx.body = data;
});

module.exports = router;