const Router = require("koa-router");
const knex = require("../../knex");

const router = new Router({ prefix: "/state" });
router.get('/', async (ctx) => {
    console.log("your requested route is /state");

    const data = await knex('state').select("*");
    ctx.body = data;
});

router.get('/:state_id', async (ctx) => {                                                                                                   
    const paramid = parseInt(ctx.params.state_id);
    console.log("your requested route is /state/:state_id");

    const data = await knex('state').select("*").where({ id: paramid }).first();
    ctx.body = data;
});

router.delete('/:state_id', async (ctx) => {
    const paramid = parseInt(ctx.params.state_id);
    console.log("your requested route is state DELETE");

    const data = await knex('state').delete().where({ id: paramid });
    ctx.body = "Deleted Id is ${paramid}";
});

router.put('/:state_id', async (ctx) => {
    const paramid = parseInt(ctx.params.state_id);
    const requestBody = ctx.request.body;
    console.log("your requested route is state PUT");

    const data = await knex('state').update(requestBody).where({ id: paramid })
        .then(async () => {
            return await knex('state').select("*").where({ id: paramid }).first();
        });

    ctx.body = data;
});

router.post('/', async (ctx) => {
    const requestBody = ctx.request.body;
    console.log("your requested route is state POST");

    const data = await knex('state').insert(requestBody);
    ctx.body = data;
});

module.exports = router;