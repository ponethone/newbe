const Router = require("koa-router");
const knex = require("../../knex");

const router = new Router({ prefix: "/township" });
router.get('/', async (ctx) => {
    console.log("your requested route is /township");

    const data = await knex('township')
        .select('township.*', 'state.state_region', 'district.district_name')
        //.from('township', 'district', 'state')
        .leftJoin('district', 'township.district_id', 'district.id')
        .join('state', 'district.state_id', 'state.id')

    ctx.body = data;
});

router.get('/:township_id', async (ctx) => {                                                                                                   
    const paramid = parseInt(ctx.params.township_id);
    console.log("your requested route is /township/:township_id");

    const data = await knex('township')
        .select('township.*', 'district.district_name', 'state.state_region')
        //.from('township', 'district', 'state')
        .where({"township.id": paramid})
        .leftJoin('district', 'township.district_id', 'district.id')
        .join('state', 'state.id', 'district.state_id')
        .first()

    ctx.body = data;
});

router.delete('/:township_id', async (ctx) => {
    const paramid = parseInt(ctx.params.township_id);
    console.log("your requested route is township DELETE");

    const data = await knex('township').delete().where({ id: paramid });
    ctx.body = "Deleted Id is ${paramid}";
});

router.put('/:township_id', async (ctx) => {
    const paramid = parseInt(ctx.params.township_id);
    const requestBody = ctx.request.body;
    console.log("your requested route is township PUT");

    console.log("Before townshipColumns")
    const townshipColumns = ['township_code', 'township_name', 'remark', 'updated_user'];
    console.log("Before ColumnsfromOtherTables")

    let townshipData = Object.keys(requestBody) 
    .filter(key => townshipColumns.includes(key))
    .reduce((obj, key) => {
        return {
        ...obj,
        [key]: requestBody[key]
        };
    }, {});

    console.log("Filtered inserted township excluding state_region and district_name: ", townshipData)

    const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    //const didfromDistrict = (await knex('district').select("id").where({ district_name: requestBody.district_name }).first()).id; // Obj.id
    const didFromDistrict = (await knex('district').select("id").where({ state_id: sidFromState, district_name: requestBody.district_name }).first()).id; // Obj.id

    townshipData = {...townshipData, district_id: didFromDistrict}
    console.log("inserted township including state_id, district_id foreign key: ", townshipData)
    
    const data = await knex('township').update(townshipData).where({ id: paramid })
        .then(async () => {
            return await knex('township').select("*").where({ id: paramid }).first();
        });

    ctx.body = data;
});

router.post('/', async (ctx) => {
    const requestBody = ctx.request.body;
    console.log("your requested route is township POST");
    console.log("requestBody is: ", requestBody)

    console.log("Before townshipColumns")
    const townshipColumns = ['township_code', 'township_name', 'remark'];
    console.log("Before ColumnsfromOtherTables")

    let townshipData = Object.keys(requestBody) 
    .filter(key => townshipColumns.includes(key))
    .reduce((obj, key) => {
        return {
        ...obj,
        [key]: requestBody[key]
        };
    }, {});

    console.log("Filtered inserted township excluding state_region and district_name: ", townshipData)

    const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    //const didfromDistrict = (await knex('district').select("id").where({ district_name: requestBody.district_name }).first()).id; // Obj.id
    const didFromDistrict = (await knex('district').select("id").where({ state_id: sidFromState, district_name: requestBody.district_name }).first()).id; // Obj.id

    townshipData = {...townshipData, district_id: didFromDistrict}
    console.log("inserted township including state_id, district_id foreign key: ", townshipData)

    const data = await knex('township').insert(townshipData);
    ctx.body = data;
});

module.exports = router;