const Router = require("koa-router");
const knex = require("../../knex");

const router = new Router({ prefix: "/office" });
router.get('/', async (ctx) => {
    console.log("your requested route is /office");

    const data = await knex('office')
        .select('office.*', 'state.state_region', 'district.district_name', 'township.township_name', 'town.town_name', 'ward.ward_name', 'village_tracts.village_tracts_name', 'village.village_name')
        .leftJoin('village', 'office.village_id', 'village.id')
        .leftJoin('village_tracts', 'office.village_tracts_id', 'village_tracts.id')
        .leftJoin('ward', 'office.ward_id', 'ward.id')
        .leftJoin('township', 'office.township_id', 'township.id')
        .join('town', 'town.township_id', 'township.id')
        .leftJoin('district', 'office.district_id', 'district.id')
        .leftJoin('state', 'office.state_id', 'state.id')

    ctx.body = data;
});

router.get('/:office_id', async (ctx) => {                                                                                                   
    const paramid = parseInt(ctx.params.office_id);
    console.log("your requested route is /office/:office_id");

    const data = await knex('office')
        .select('office.*', 'state.state_region', 'district.district_name', 'township.township_name', 'town.town_name', 'ward.ward_name', 'village_tracts.village_tracts_name', 'village.village_name')
        .where({ id: paramid })
        .leftJoin('village', 'office.village_id', 'village.id')
        .leftJoin('village_tracts', 'office.village_tracts_id', 'village_tracts.id')
        .leftJoin('ward', 'office.ward_id', 'ward.id')
        .leftJoin('township', 'office.township_id', 'township.id')
        .join('town', 'town.township_id', 'township.id')
        .leftJoin('district', 'office.district_id', 'district.id')
        .leftJoin('state', 'office.state_id', 'state.id')
        .first()

    ctx.body = data;
});

router.delete('/:office_id', async (ctx) => {
    const paramid = parseInt(ctx.params.office_id);
    console.log("your requested route is office DELETE");

    const data = await knex('office').delete().where({ id: paramid });
    ctx.body = "Deleted Id is ${paramid}";
});

router.put('/:office_id', async (ctx) => {
    const paramid = parseInt(ctx.params.office_id);
    const requestBody = ctx.request.body;
    console.log("your requested route is office PUT");

    console.log("Before wardColumns")
    const officeColumns = ['office_name', 'address', 'ph_no1', 'ph_no2', 'email', 'building_type', 'own_rental', 'updated_user'];

    let officeData = Object.keys(requestBody) 
    .filter(key => officeColumns.includes(key))
    .reduce((obj, key) => {
        return {
        ...obj,
        [key]: requestBody[key]
        };
    }, {});

    console.log("Filtered inserted town excluding state_region, district_name, and town_name: ", officeData)

    const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    const didFromDistrict = (await knex('district').select("id").where({ district_name: requestBody.district_name }).first()).id;
    const tsidFromTownship = (await knex('township').select("id").where({ township_name: requestBody.township_name }).first()).id;
    const tidFromTown = (await knex('town').select("id").where({ town_name: requestBody.town_name }).first()).id; // Obj.id
    const widFromWard = (await knex('ward').select("id").where({ town_id: tidFromTown, ward_name: requestBody.ward_name }).first()).id;
    const vtidFromVillage_Tracts = (await knex('village_tracts').select("id").where({ township_id: tidFromTown, village_tracts_name: requestBody.village_tracts_name }).first()).id;
    const vidFromVillage = (await knex('village').select("id").where({ village_tracts_id: vtidFromVillage_Tracts, village_name: requestBody.village_name }).first()).id;
    
    officeData = {
        ...officeData, 
        state_id: sidFromState, 
        district_id: didFromDistrict, 
        township_id: tsidFromTownship, 
        ward_id: widFromWard, 
        village_tracts_id: vtidFromVillage_Tracts, 
        village_id: vidFromVillage
    }
    console.log("inserted township including state_id, district_id foreign key: ", officeData)

    const data = await knex('office').update(officeData).where({ id: paramid })
        .then(async () => {
            return await knex('office').select("*").where({ id: paramid }).first();
        });

    ctx.body = data;
});

router.post('/', async (ctx) => {
    const requestBody = ctx.request.body;
    console.log("your requested route is office POST");

    console.log("Before wardColumns")
    const officeColumns = ['office_name', 'address', 'ph_no1', 'ph_no2', 'email', 'building_type', 'own_rental'];

    let officeData = Object.keys(requestBody) 
    .filter(key => officeColumns.includes(key))
    .reduce((obj, key) => {
        return {
        ...obj,
        [key]: requestBody[key]
        };
    }, {});

    console.log("Filtered inserted town excluding state_region, district_name, and town_name: ", officeData)

    // const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    // const didFromDistrict = (await knex('district').select("id").where({ district_name: requestBody.district_name }).first()).id;
    // const tsidFromTownship = (await knex('township').select("id").where({ township_name: requestBody.township_name }).first()).id;
    // const tidFromTown = (await knex('town').select("id").where({ town_name: requestBody.town_name }).first()).id; // Obj.id
    // //const widFromWard = (await knex('ward').select("id").where({ ward_name: requestBody.ward_name }).first()).id;
    // const widFromWard = (await knex('ward').select("id").where({ town_id: tidFromTown, ward_name: requestBody.ward_name }).first()).id;
    // const vtidFromVillage_Tracts = (await knex('village_tracts').select("id").where({ village_tracts_name: requestBody.village_tracts_name }).first()).id;
    // const vidFromVillage = (await knex('village').select("id").where({ village_name: requestBody.village_name }).first()).id;

    const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    const didFromDistrict = (await knex('district').select("id").where({ district_name: requestBody.district_name }).first()).id;
    const tsidFromTownship = (await knex('township').select("id").where({ township_name: requestBody.township_name }).first()).id;
    const tidFromTown = (await knex('town').select("id").where({ town_name: requestBody.town_name }).first()).id; // Obj.id
    //const widFromWard = (await knex('ward').select("id").where({ ward_name: requestBody.ward_name }).first()).id;
    const widFromWard = (await knex('ward').select("id").where({ town_id: tidFromTown, ward_name: requestBody.ward_name }).first()).id;
    const vtidFromVillage_Tracts = (await knex('village_tracts').select("id").where({ village_tracts_name: requestBody.village_tracts_name }).first()).id;
    const vidFromVillage = (await knex('village').select("id").where({ village_tracts_id: vtidFromVillage_Tracts, village_name: requestBody.village_name }).first()).id;
    
    officeData = {
        ...officeData, 
        state_id: sidFromState, 
        district_id: didFromDistrict, 
        township_id: tsidFromTownship, 
        ward_id: widFromWard, 
        village_tracts_id: vtidFromVillage_Tracts, 
        village_id: vidFromVillage
    }
    console.log("inserted township including state_id, district_id foreign key: ", officeData)

    const data = await knex('office').insert(officeData);
    ctx.body = data;
});

module.exports = router;