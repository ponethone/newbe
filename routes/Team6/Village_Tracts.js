const Router = require("koa-router");
const knex = require("../../knex");

const router = new Router({ prefix: "/village_tracts" });
router.get('/', async (ctx) => {
    console.log("your requested route is /village_tracts");

    const data = await knex('village_tracts')
        .select('village_tracts.*', 'township.township_name', 'district.district_name', 'state.state_region')
        //.from('township', 'district', 'state')
        .leftJoin('township', 'village_tracts.township_id', 'township.id')
        .join('district', 'district.id', 'township.district_id')
        .join('state', 'state.id', 'district.state_id')

    // const data = await knex('village_tracts').select("*");

    ctx.body = data;
});

router.get('/:village_tracts_id', async (ctx) => {                                                                                                   
    const paramid = parseInt(ctx.params.village_tracts_id);
    console.log("your requested route is /village_tracts/:village_tracts_id");

    const data = await knex('village_tracts').select("*").where({ id: paramid }).first();
    ctx.body = data;
});

router.delete('/:village_tracts_id', async (ctx) => {
    const paramid = parseInt(ctx.params.village_tracts_id);
    console.log("your requested route is village_tracts DELETE");

    const data = await knex('village_tracts').delete().where({ id: paramid });
    ctx.body = "Deleted Id is ${paramid}";
});

router.put('/:village_tracts_id', async (ctx) => {
    const paramid = parseInt(ctx.params.village_tracts_id);
    const requestBody = ctx.request.body;
    console.log("your requested route is village_tracts PUT");

    console.log("Before villageTractsColumns")
    const villageTractsColumns = ['village_tracts_code', 'village_tracts_name', 'remark', 'updated_user'];
    console.log("Before ColumnsfromOtherTables")

    let villageTractsData = Object.keys(requestBody) 
    .filter(key => villageTractsColumns.includes(key))
    .reduce((obj, key) => {
        return {
        ...obj,
        [key]: requestBody[key]
        };
    }, {});

    console.log("Filtered inserted town excluding state_region, district_name, and township_name: ", villageTractsData)

    const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    const didFromDistrict = (await knex('district').select("id").where({ state_id: sidFromState, district_name: requestBody.district_name }).first()).id; // Obj.id
    const tsidFromTownship = (await knex('township').select("id").where({ district_id: didFromDistrict, township_name: requestBody.township_name }).first()).id;

    villageTractsData = {...villageTractsData, township_id: tsidFromTownship}
    console.log("inserted village_tracts including state_id, district_id foreign key: ", villageTractsData)

    const data = await knex('village_tracts').update(villageTractsData).where({ id: paramid })
        .then(async () => {
            return await knex('village_tracts').select("*").where({ id: paramid }).first();
        });

    ctx.body = data;
});

router.post('/', async (ctx) => {
    const requestBody = ctx.request.body;
    console.log("your requested route is village_tracts POST");

    console.log("Before townshipColumns")
    const villageTractsColumns = ['village_tracts_code', 'village_tracts_name', 'remark', 'created_user'];
    console.log("Before ColumnsfromOtherTables")
    const columnsfromOtherTables = ['state_region', 'district_name', 'township_name'];

    let villageTractsData = Object.keys(requestBody) 
    .filter(key => villageTractsColumns.includes(key))
    .reduce((obj, key) => {
        return {
        ...obj,
        [key]: requestBody[key]
        };
    }, {});

    console.log("Filtered inserted town excluding state_region, district_name, and township_name: ", villageTractsData)

    const sidFromState = (await knex('state').select("id").where({ state_region: requestBody.state_region }).first()).id; // Obj.id
    console.log("sidFromState: "); console.log(sidFromState)
    const didFromDistrict = (await knex('district').select("id").where({ state_id: sidFromState, district_name: requestBody.district_name }).first()).id; // Obj.id
    console.log("didFromDistrict: "); console.log(didFromDistrict)
    const tsidFromTownship = (await knex('township').select("id").where({ district_id: didFromDistrict, township_name: requestBody.township_name }).first()).id;
    console.log("tsidFromTownship: "); console.log(tsidFromTownship)

    villageTractsData = {...villageTractsData, township_id: tsidFromTownship}
    console.log("inserted village_tracts including state_id, district_id foreign key: ", villageTractsData)

    const data = await knex('village_tracts').insert(villageTractsData);
    ctx.body = data;
});

module.exports = router;


// Original
// const Router = require("koa-router");
// const knex = require("../../knex");

// const router = new Router({ prefix: "/village_tracts" });
// router.get('/', async (ctx) => {
//     console.log("your requested route is /village_tracts");

//     // const data = await knex('village_tracts')  
//     // .select('village_tracts.*', 'township.township_name', 'district.district_name', 'state.state_region')
//     // .leftJoin('township', 'village_tracts.id', 'township.id')
//     // .join('district', 'township.district_id', 'district.id')
//     // .join('state', 'district.state_id', 'state.id');

//     const data = await knex('village_tracts').select("*");

//     ctx.body = data;
// });

// router.get('/:village_tracts_id', async (ctx) => {                                                                                                   
//     const paramid = parseInt(ctx.params.village_tracts_id);
//     console.log("your requested route is /village_tracts/:village_tracts_id");

//     const data = await knex('village_tracts').select("*").where({ id: paramid }).first();
//     ctx.body = data;
// });

// router.delete('/:village_tracts_id', async (ctx) => {
//     const paramid = parseInt(ctx.params.village_tracts_id);
//     console.log("your requested route is village_tracts DELETE");

//     const data = await knex('village_tracts').delete().where({ id: paramid });
//     ctx.body = "Deleted Id is ${paramid}";
// });

// router.put('/:village_tracts_id', async (ctx) => {
//     const paramid = parseInt(ctx.params.village_tracts_id);
//     const requestBody = ctx.request.body;
//     console.log("requestBody: ", requestBody)
//     console.log("your requested route is village_tracts PUT");

//     const includingTSData = {...requestBody, township_id: 1}

//     const data = await knex('village_tracts').update(requestBody).where({ id: paramid })
//         .then(async () => {
//             return await knex('village_tracts').select("*").where({ id: paramid }).first();
//         });

//     ctx.body = data;
// });

// router.post('/', async (ctx) => {
//     const requestBody = ctx.request.body;
//     console.log("your requested route is village_tracts POST");

//     const data = await knex('village_tracts').insert(requestBody);
//     ctx.body = data;
// });

// module.exports = router;